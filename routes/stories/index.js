import {
	withDBConnection,
	withAdminDBConnection,
	isLoggedIn
} from '../util';
import * as storyRoutes from './StoriesController';

export default app => {
	app.route('/api/stories/pictures')
		.get(isLoggedIn, withDBConnection(storyRoutes.getAllPictures));

	app.route('/api/stories/stories')
		.get(isLoggedIn, withDBConnection(storyRoutes.getAllStories));

	app.route('/api/stories/:storyId/references')
		.get(isLoggedIn, withDBConnection(storyRoutes.getStoryReferences));

	app.route('/api/stories/:storyId/references')
		.post(isLoggedIn, withAdminDBConnection(storyRoutes.addStoryReference));

	app.route('/api/stories/:storyId/references/:outletId')
		.delete(isLoggedIn, withAdminDBConnection(storyRoutes.removeStoryReference));

	app.route('/api/stories/outlets/search')
		.get(isLoggedIn, withDBConnection(storyRoutes.searchStoryOutlets));

	app.route('/api/stories/:storyId')
		.get(isLoggedIn, withDBConnection(storyRoutes.getStory));
}
