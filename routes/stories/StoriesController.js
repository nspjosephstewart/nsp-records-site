import * as STATUS_CODES from 'http-status-codes';

import moment from 'moment';

import {
	returnSQLResponse,
	promiseQuery,
	getPermissions,
} from '../util';

/**
 * Shared sql for pictures and stories search queries
 */
const fullJoin = `
LEFT JOIN
rallies ON rallies.id = califo_storycapture.Story.rally_id
	LEFT JOIN
witnessing_day ON witnessing_day.id = califo_storycapture.Story.witnessing_day_id
	LEFT JOIN
school_registration ON school_registration.registration_id = COALESCE(rallies.schoolreg_id, witnessing_day.schoolreg_id)
	LEFT JOIN
schools ON school_registration.school_id = schools.id
	LEFT JOIN
chapters ON chapters.id = COALESCE(witnessing_day.chapter_id, school_registration.chapter_id)
`;

/**
 * Shared sql for pictures and stories search queries
 */
const fullSelect = `
,schools.name AS school,
chapters.name AS chapter,
schools.state,
(
	SELECT COUNT(*) FROM
		califo_storycapture.StoryReference
	WHERE califo_storycapture.StoryReference.story_id = califo_storycapture.Story.story_id
) AS total_references,
(
	SELECT COUNT(*) FROM
		califo_storycapture.StoryReference
			JOIN
		califo_storycapture.StoryOutlet ON califo_storycapture.StoryReference.outlet_id = califo_storycapture.StoryOutlet.outlet_id
	WHERE StoryReference.story_id = Story.story_id AND StoryOutlet.type = 'e'
) AS event_references,
(
	SELECT COUNT(*) FROM
		califo_storycapture.StoryReference
			JOIN
		califo_storycapture.StoryOutlet ON califo_storycapture.StoryReference.outlet_id = califo_storycapture.StoryOutlet.outlet_id
	WHERE califo_storycapture.StoryReference.story_id = califo_storycapture.Story.story_id AND califo_storycapture.StoryOutlet.type = 's'
) AS social_media_references,
(
	SELECT COUNT(*) FROM 
		califo_storycapture.StoryReference 
			JOIN 
		califo_storycapture.StoryOutlet ON califo_storycapture.StoryReference.outlet_id = califo_storycapture.StoryOutlet.outlet_id
	WHERE califo_storycapture.StoryReference.story_id = califo_storycapture.Story.story_id AND califo_storycapture.StoryOutlet.type = 'p'
) AS publication_references,
(
	SELECT GROUP_CONCAT(califo_storycapture.StoryOutlet.name) FROM 
		califo_storycapture.StoryReference 
			JOIN 
		califo_storycapture.StoryOutlet ON califo_storycapture.StoryReference.outlet_id = califo_storycapture.StoryOutlet.outlet_id
	WHERE califo_storycapture.StoryReference.story_id = califo_storycapture.Story.story_id AND califo_storycapture.StoryOutlet.type = 'p'
) AS publication_names,
(
	SELECT GROUP_CONCAT(califo_storycapture.StoryOutlet.name) FROM 
		califo_storycapture.StoryReference 
			JOIN 
		califo_storycapture.StoryOutlet ON califo_storycapture.StoryReference.outlet_id = califo_storycapture.StoryOutlet.outlet_id
	WHERE califo_storycapture.StoryReference.story_id = califo_storycapture.Story.story_id AND califo_storycapture.StoryOutlet.type = 's'
) AS social_media_names,
(
	SELECT GROUP_CONCAT(califo_storycapture.StoryOutlet.name) FROM 
		califo_storycapture.StoryReference 
			JOIN 
		califo_storycapture.StoryOutlet ON califo_storycapture.StoryReference.outlet_id = califo_storycapture.StoryOutlet.outlet_id
	WHERE califo_storycapture.StoryReference.story_id = califo_storycapture.Story.story_id AND califo_storycapture.StoryOutlet.type = 'e'
) AS event_names
`

/**
 * @description Formats the filters for SQL use
 */
const getFilters = filters => {
	if (!filters) {
		return {
			filtersSQL: '',
			sqlFilterValues: {},
		}
	}

	const sqlFilterArray = [];
	const sqlFilterValues = {};
	const filtersArray = filters.split(/\s*,\s*/i);

	for (let i = 0; i < filtersArray.length; i++) {
		const [
			columnName,
			value,
			type,
		] = filtersArray[i].split(':');

		sqlFilterValues[`filterCol${i}`] = columnName;

		if (type === 'in') {
			sqlFilterArray.push(`:(filterCol${i}) IN (:filterVal${i})`);
			sqlFilterValues[`filterVal${i}`] = value.split(/\s*\+\s*/i);
		} else if (type === 'not_in') {
			sqlFilterArray.push(`:(filterCol${i}) NOT IN (:filterVal${i})`);
			sqlFilterValues[`filterVal${i}`] = value.split(/\s*\+\s*/i);
		} else if (type === 'like') {
			sqlFilterArray.push(`:(filterCol${i}) LIKE :filterVal${i}`);
			sqlFilterValues[`filterVal${i}`] = `%${value}%`;
		} else if (type === 'not_like') {
			sqlFilterArray.push(`NOT :(filterCol${i}) LIKE :filterVal${i}`);
			sqlFilterValues[`filterVal${i}`] = `%${value}%`;
		} else if (type === 'is') {
			sqlFilterArray.push(`:(filterCol${i}) = :filterVal${i}`);
			sqlFilterValues[`filterVal${i}`] = value;
		} else if (type === 'is_not') {
			sqlFilterArray.push(`NOT :(filterCol${i}) = :filterVal${i}`);
			sqlFilterValues[`filterVal${i}`] = value;
		}
	}

	return {
		filtersSQL: `AND (${sqlFilterArray.join(' AND ')})`,
		sqlFilterValues,
	};
}

/**
 * @description Returns all pictures either attached to a story or submitted through story capture
 */
export const getAllPictures = (request, response, connection) => {
	const { includeStoryCapture, limit, full, filters, query } = request.query;

	const limitSQLSection = limit ? 'LIMIT :limit' : '';
	const { filtersSQL, sqlFilterValues } = getFilters(filters);

	const querySection = query ? `
		AND text LIKE :query
	` : '';

	connection.query(
		`
			SELECT * FROM (
				SELECT
					story_id AS id,
					text,
					COALESCE(rally_id, witnessing_day_id, story_id) AS source_id,
					califo_storycapture.Story.picture_url,
					califo_storycapture.Story.timestamp,
					IF(
						rally_id IS NOT NULL,
						'rallies',
						IF(
							witnessing_day_id IS NOT NULL,
							'witnessing',
							'story'
						)
					) AS type,
					posted_by_user_name
					${full ? fullSelect : ''}
				FROM
					califo_storycapture.Story
					${full ? fullJoin : ''}
			) T
			WHERE
				T.timestamp IS NOT NULL
				AND NOT picture_url LIKE "https://drive.google.com%"
				${includeStoryCapture ? 'AND NOT T.type = \'story\'' : ''}
				${filtersSQL}
				${querySection}
			ORDER BY T.timestamp DESC
			${limitSQLSection};
		`,
		{ query: `%${query}%`, limit: parseInt(limit), ...sqlFilterValues },
		returnSQLResponse(connection, response, results => (
			results.map(obj => {
				if (obj.picture_url.startsWith('http')) {
					return obj;
				}
				return {
					...obj,
					picture_url: `https://drive.google.com/uc?export=view&id=${obj.picture_url.split(',')[0].trim()}`
				}
			})
		))
	)
}

export const getAllStories = (request, response, connection) => {
	const { includeStoryCapture, limit, query, full, filters } = request.query;

	const limitSQLSection = limit ? 'LIMIT :limit' : '';
	const querySection = query ? `
		AND text LIKE :query
	` : '';

	const { filtersSQL, sqlFilterValues } = getFilters(filters);

	connection.query(
		`
			SELECT * FROM (
				SELECT
					story_id AS id,
					COALESCE(rally_id, witnessing_day_id, story_id) AS source_id,
					text,
					califo_storycapture.Story.timestamp,
					IF(
						rally_id IS NOT NULL,
						'rallies',
						IF(
							witnessing_day_id IS NOT NULL,
							'witnessing',
							'story'
						)
					) AS type,
					posted_by_user_name
					${full ? fullSelect : ''}
				FROM
					califo_storycapture.Story
					${full ? fullJoin : ''}
			) T
			WHERE
				T.text IS NOT NULL
				${includeStoryCapture ? 'AND NOT T.type = \'story\'' : ''}
				${querySection}
				${filtersSQL}
			ORDER BY T.timestamp DESC
			${limitSQLSection};
		`,
		{ limit: parseInt(limit), query: `%${query}%`, ...sqlFilterValues },
		returnSQLResponse(connection, response)
	)
}

/**
 * @description Returns a singular story
 */
export const getStory = (request, response, connection) => {
	const { storyId } = request.params;

	connection.query(
		`
			SELECT
				Story.picture_url,
				posted_by_user_name,
				Story.timestamp,
				text,
				COALESCE(rally_id, witnessing_day_id, story_id) AS source_id,
				schools.name AS school_name,
				schools.id AS school_id,
				Story.story_id AS story_id,
				IF(
					rally_id IS NOT NULL,
					'rallies',
					IF(
						witnessing_day_id IS NOT NULL,
						'witnessing',
						'story'
					)
				) AS type
			FROM
				califo_storycapture.Story
					LEFT JOIN
				rallies ON rallies.id = califo_storycapture.Story.rally_id
					LEFT JOIN
				witnessing_day ON witnessing_day.id = califo_storycapture.Story.witnessing_day_id
					LEFT JOIN
				school_registration ON school_registration.registration_id = COALESCE(rallies.schoolreg_id, witnessing_day.schoolreg_id)
					LEFT JOIN
				schools ON school_registration.school_id = schools.id
					LEFT JOIN
				chapters ON chapters.id = COALESCE(witnessing_day.chapter_id, school_registration.chapter_id)
			WHERE
				Story.story_id = :storyId
		`,
		{ storyId },
		returnSQLResponse(connection, response, results => 
			results.map(result => ({
				...result,
				picture_url: undefined,
				picture_urls: result.picture_url ? 
					result
						.picture_url
						.split(/\s*,\s*/)
						.map(picture_url => {
							if (/^https?:\/\//.test(picture_url)) {
								return picture_url;
							}
							return `https://drive.google.com/uc?export=view&id=${picture_url}`
						})
					:
					[]
			}))
		)
	);
}

/**
 * @description Gets all references of a singular story
 */
export const getStoryReferences = (request, response, connection) => {
	const { storyId } = request.params;

	connection.query(
		`
			SELECT
				name,
				StoryOutlet.outlet_id,
				StoryOutlet.date,
				type,
				attendance_count,
				speaker,
				medium,
				print_count,
				url
			FROM
				califo_storycapture.StoryReference
					JOIN
				califo_storycapture.StoryOutlet ON StoryReference.outlet_id = StoryOutlet.outlet_id
					LEFT JOIN
				califo_storycapture.Event ON StoryOutlet.outlet_id = Event.outlet_id
					LEFT JOIN
				califo_storycapture.Publication ON Publication.outlet_id = StoryOutlet.outlet_id
					LEFT JOIN
				califo_storycapture.SocialMediaPlatform ON SocialMediaPlatform.outlet_id = StoryOutlet.outlet_id
			WHERE
				StoryReference.story_id = :storyId
		`,
		{ storyId },
		returnSQLResponse(connection, response)
	)
}

/**
 * @description Removes a reference to story
 */
export const removeStoryReference = async (request, response, connection) => {
	const { storyId, outletId } = request.params;
	const { isAdmin } = getPermissions(request);
	
	if (!isAdmin) {
		return response.status(STATUS_CODES.UNAUTHORIZED).json({ reason: 'You are not an admin user' });
	}

	connection.query(
		`
			DELETE FROM califo_storycapture.StoryReference
			WHERE
				story_id = :storyId AND
				outlet_id = :outletId
		`,
		{ storyId, outletId },
		returnSQLResponse(connection, response)
	)
}

/**
 * @description Searches existing outlets
 */
export const searchStoryOutlets = async (request, response, connection) => {
	const { query } = request.query;

	connection.query(
		`
			SELECT name, outlet_id FROM
				califo_storycapture.StoryOutlet
			${query ?
				`WHERE
					name LIKE :query
				`
				:
				''
			}
			LIMIT 10
		`,
		{ query: `%${query}%` },
		returnSQLResponse(connection, response)
	);
};

/**
 * @description Add new story reference
 */
export const addStoryReference = async (request, response, connection) => {
	const { storyId } = request.params;
	const {
		selectedOutlet,
		creatingNew,
		type,
		url,
		printCount,
		medium,
		attendanceCount,
		speaker,
		date,
	} = request.body;
	const { isAdmin } = getPermissions(request);

	let outletId = selectedOutlet.value;

	if (!isAdmin) {
		return response.status(STATUS_CODES.UNAUTHORIZED).json({ reason: 'You are not an admin user' });
	}

	if (!creatingNew && (!selectedOutlet.label || !date)) {
		return response.status(STATUS_CODES.BAD_REQUEST).json({ reason: 'If you are creating a new story outlet, you must provide a name and date' });
	}

	if (creatingNew) {
		const outletName = selectedOutlet.label;

		const insertNewOutletResponse = await promiseQuery(
			connection,
			`
				INSERT INTO califo_storycapture.StoryOutlet
					(name, date, type)
				VALUES
					(:outletName, :date, :type)
			`,
			{
				outletName,
				date: moment(date).format('YYYY-MM-DD'),
				type,
			}
		);

		outletId = insertNewOutletResponse.insertId

		if (type === 's') {
			await promiseQuery(
				connection,
				`
					INSERT INTO califo_storycapture.SocialMediaPlatform
						(outlet_id, url)
					VALUES
						(:outletId, :url)
				`,
				{ outletId, url }
			);
		} else if (type === 'e') {
			await promiseQuery(
				connection,
				`
					INSERT INTO califo_storycapture.Event
						(outlet_id, speaker, attendance_count)
					VALUES
						(:outletId, :speaker, :attendanceCount)
				`,
				{ outletId, speaker, attendanceCount }
			);
		} else if (type === 'p') {
			await promiseQuery(
				connection,
				`
					INSERT INTO califo_storycapture.Publication
						(outlet_id, medium, print_count)
					VALUES
						(:outletId, :medium, :printCount)
				`,
				{ outletId, medium, printCount }
			)
		}
	}

	connection.query(
		`
			INSERT INTO califo_storycapture.StoryReference
				(story_id, outlet_id)
			VALUES
				(:storyId, :outletId)
		`,
		{ storyId, outletId },
		returnSQLResponse(connection, response)
	)
}
