import * as STATUS_CODES from 'http-status-codes';
import {
	returnSQLResponse,
	uploadImages,
	promiseQuery,
} from '../util';

export const postRallyForm = async (request, response, connection) => {
	const { files, body } = request;

	let fileUrls;
	try {
		fileUrls = await uploadImages(files, '/rallies');
	} catch (err) {
		return response.status(STATUS_CODES.INTERNAL_SERVER_ERROR).json(err);
	}

	const insertObj = { ...body, ...fileUrls };

	try {
		await promiseQuery(
			connection,
			`
				INSERT INTO rallies
				(timestamp, card_explained, card_explainedby, club_attendance, collected_data, was_contact_cards, daily_theme, date, stories, indicated_decisions, location, lunch_periods, materials_bibles, was_materials_distributed, materials_gospel_booklets, materials_gospels_of_john, materials_lifebooks, materials_other, materials_other_type, was_materials_table, performer_email, performer_name, performer_organization, performer_phone, performer_specialty, performer_stagename, performer_student_guest, was_performer, performer_effectiveness, speaker_email, speaker_engagement, speaker_explained_saved, speaker_gospel_clear, speaker_gospel_shared, speaker_invitation, speaker_isperformer, speaker_name, speaker_organization, speaker_phone, speaker_physical_invitation, speaker_student_guest, speaker_topic, speaker_topically_known, was_speaker, student_names_participating, students_participating, time, type, week_theme, total_attendance, speaker_explained_saved_clear, schoolreg_id, picture_url)
				VALUES
				(CURRENT_TIMESTAMP, :card_explained, :card_explainedby, :club_attendance, :collected_data, :was_contact_cards, :daily_theme, :date, :stories, :indicated_decisions, :location, :lunch_periods, :materials_bibles, :was_materials_distributed, :materials_gospel_booklets, :materials_gospels_of_john, :materials_lifebooks, :materials_other, :materials_other_type, :was_materials_table, :performer_email, :performer_name, :performer_organization, :performer_phone, :performer_specialty, :performer_stagename, :performer_student_guest, :was_performer, :performer_effectiveness, :speaker_email, :speaker_engagement, :speaker_explained_saved, :speaker_gospel_clear, :speaker_gospel_shared, :speaker_invitation, :speaker_isperformer, :speaker_name, :speaker_organization, :speaker_phone, :speaker_physical_invitation, :speaker_student_guest, :speaker_topic, :speaker_topically_known, :was_speaker, :student_names_participating, :students_participating, :time, :type, :week_theme, :total_attendance, :speaker_explained_saved_clear, :schoolreg_id, :"picture_url")
			`,
			insertObj,
		);

		if (insertObj.was_contact_cards) {
			await promiseQuery(
				connection,
				`
					INSERT INTO contact_cards
					(date, collected_data, schoolreg_id, total_cards, unique_cards, unique_contact_info, first_time_decisions, first_time_followup, recommitments, recommitments_followup, not_ready_followup, total_followup, timestamp)
					VALUES
					(:date, :collected_data, :schoolreg_id, :total_cards, :unique_cards, :unique_contact_info, :first_time_decisions, :first_time_followup, :recommitments, :recommitments_followup, :not_ready_followup, :total_followup, :timestamp)
				`,
				insertObj,
			);
		}

		return response.status(STATUS_CODES.OK).json({ message: 'You\'re good to go!' });
	} catch (err) {
		return response.status(STATUS_CODES.INTERNAL_SERVER_ERROR).json({ err });
	}
}