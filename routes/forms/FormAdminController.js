import * as STATUS_CODES from 'http-status-codes';
import {
	getPermissions,
	returnSQLResponse,
} from '../util';

export const getFormsForAdmin = (request, response, connection) => {
	const { isAdmin } = getPermissions(request);
	if (!isAdmin) {
		return response.status(STATUS_CODES.UNAUTHORIZED).json({ reason: 'You are not an admin user' });
	}

	connection.query(`
		SELECT title, id, active, creator, custom_handler, name FROM forms
	`, 
	returnSQLResponse(connection, response))
}

export const postNewForm = (request, response, connection) => {
	const { isAdmin } = getPermissions(request);
	const { name } = request.body;
	const { name: userName, email } = request.user;

	if (!isAdmin) {
		return response.status(STATUS_CODES.UNAUTHORIZED).json({ reason: 'You are not an admin user' });
	}

	if (!name || typeof name !== 'string') {
		return response.status(STATUS_CODES.BAD_REQUEST).json({ reason: 'Your form must have a name' });
	}

	const idName = name.split(' ').filter(item => item).join('-').toLowerCase();

	connection.query(`
		INSERT INTO forms
		(title, pages, creator, creator_email, active, name)
		VALUES
		(:name, '[]', :userName, :email, 0, :idName)
	`,
		{ name, userName, email, idName },
		returnSQLResponse(connection, response, result => ({ id: result.insertId }))
	);
}

export const getFormForEditing = (request, response, connection) => {
	const { id } = request.params;
	const { isAdmin } = getPermissions(request);

	if (!isAdmin) {
		return response.status(STATUS_CODES.UNAUTHORIZED).json({ reason: 'You are not an admin user' }); 
	}

	connection.query(`
		SELECT * FROM forms WHERE id = :id
	`,
	{id},
	returnSQLResponse(connection, response, results => results[0] || {}))
}

export const activateForm = (request, response, connection) => {
	const { id } = request.params;
	const { isAdmin } = getPermissions(request);

	if (!isAdmin) {
		return response.status(STATUS_CODES.UNAUTHORIZED).json({ reason: 'You are not an admin user' });
	}
	
	connection.query(`
		UPDATE forms SET active = 1 WHERE id = :id
	`,
	{id},
	returnSQLResponse(connection, response, results => ({ changed: (results.changedRows > 0) })));
}

export const deactivateForm = (request, response, connection) => {
	const { id } = request.params;
	const { isAdmin } = getPermissions(request);

	if (!isAdmin) {
		return response.status(STATUS_CODES.UNAUTHORIZED).json({ reason: 'You are not an admin user' });
	}
	
	connection.query(`
		UPDATE forms SET active = 0 WHERE id = :id
	`,
	{id},
	returnSQLResponse(connection, response, results => ({ changed: (results.changedRows > 0) })));
}

export const updateForm = (request, response, connection) => {
	const { id } = request.params;
	const { isAdmin } = getPermissions(request);
	const { body } = request;

	if (!isAdmin) {
		return response.status(STATUS_CODES.UNAUTHORIZED).json({ reason: 'You are not an admin user' });
	}

	connection.query(`
		UPDATE forms SET
			pages = :pages,
			title = :title,
			subtitle = :subtitle,
			active = :active,
			name = :name
		WHERE id = :id
	`, {
		...{
			...body,
			pages: JSON.stringify(body.pages),
		},
		id
	}, returnSQLResponse(connection, response))
}