import multer from 'multer';

import {
	withDBConnection,
	isLoggedIn,
	hasFormsAccess,
	withAdminDBConnection,
	getHomeDirectory,
} from '../util';

import * as formRoutes from './FormsController';
import * as formAdminRoutes from './FormAdminController';
import * as rallyFormRoutes from './RallyForm';

const upload = multer({ storage: multer.memoryStorage() });

export default app => {
	// Basic 

	app.route('/api/forms/post')
		.post(hasFormsAccess, upload.any(), withAdminDBConnection(formRoutes.postGenericForm));

	app.route('/api/forms/post/witnessing')
		.post(hasFormsAccess, upload.any(), withAdminDBConnection(formRoutes.postWitnessingForm));

	app.route('/api/forms/post/rally')
		.post(hasFormsAccess, upload.any(), withAdminDBConnection(rallyFormRoutes.postRallyForm));

	app.route('/api/forms/:name')
		.get(hasFormsAccess, withDBConnection(formRoutes.getForm));

	app.route('/api/forms')
		.get(hasFormsAccess, withDBConnection(formRoutes.getForms));
	
	app.route('/api/forms')
		.get(withDBConnection(formRoutes.getForms));

	
	// Form admin routes

	app.route('/api/admin/forms/:id/activate')
		.get(isLoggedIn, withAdminDBConnection(formAdminRoutes.activateForm));

	app.route('/api/admin/forms/:id/deactivate')
		.get(isLoggedIn, withAdminDBConnection(formAdminRoutes.deactivateForm));

	app.route('/api/admin/forms/:id')
		.get(isLoggedIn, withDBConnection(formAdminRoutes.getFormForEditing));

	app.route('/api/admin/forms')
		.get(isLoggedIn, withDBConnection(formAdminRoutes.getFormsForAdmin));

	app.route('/api/admin/forms')
		.post(isLoggedIn, withAdminDBConnection(formAdminRoutes.postNewForm));

	// Tell the server to route forms traffic to the forms page
	app.route('/forms').get(hasFormsAccess, (request, response) => {
		response.sendFile('client/dist/forms.html', { root: getHomeDirectory() });
	});
	app.route('/api/admin/forms/:id')
		.post(isLoggedIn, withAdminDBConnection(formAdminRoutes.updateForm));	

	// Tell the server to route forms traffic to the forms page
	app.route('/forms/*').get(hasFormsAccess, (request, response) => {
		response.sendFile('client/dist/forms.html', { root: getHomeDirectory() });
	});
}