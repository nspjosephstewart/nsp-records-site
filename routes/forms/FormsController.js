import * as STATUS_CODES from 'http-status-codes';
import {
	returnSQLResponse,
	uploadImages,
} from '../util';
import logger from '../../logger'

const easterEggPeople = [
	'joseph.s@nationalschoolproject.com',
	'brian.j@nationalschoolproject.com',
	'tommy.s@nationalschoolproject.com',
	'benjamin.m.vincent@biola.edu',
	'benjamin.lee@biola.edu',
	'jaston@nationalschoolproject.com',
	'thomas.s.shirota@biola.edu',
	'brian.t.johnson@biola.edu',
];

const formatEasterEgg = questions => {
	questions = questions.replace(/NSP Members?/gi, 'NSPeople');
	questions = questions.replace(/NSP community/gi, 'NSPals');
	return questions;
};

export const getForm = (request, response, connection) => {
	const { name } = request.params;

	connection.query(`
		SELECT title, subtitle, pages, creator, creator_email, custom_handler, name FROM forms
		WHERE
			name = :name AND active = 1
	`,
	{name},
	returnSQLResponse(connection, response, results => {
		if (easterEggPeople.includes(request.user.email)) {
			results[0].pages = formatEasterEgg(results[0].pages);
		}
		return results[0] || {};
	}))
}

export const getForms = (request, response, connection) => {
	connection.query(
		`SELECT title, name, thumbnail_url FROM forms WHERE active = 1`,
		returnSQLResponse(connection, response)
	);
}

export const postGenericForm = (request, response, connection) => {
	const { name } = request.query;
	if (!name) {
		return response.status(STATUS_CODES.BAD_REQUEST).end();
	}
	response.status(STATUS_CODES.NOT_IMPLEMENTED).end();
}

export const postWitnessingForm = async (request, response, connection) => {
	const { files, body } = request;
	let fileUrls;
	try {
		fileUrls = await uploadImages(files, '/witnessing');
	} catch (err) {
		logger.error(response.status(STATUS_CODES.INTERNAL_SERVER_ERROR).json(err))
		return response.status(STATUS_CODES.INTERNAL_SERVER_ERROR).json(err);
	}

	const insertObj = { ...body, ...fileUrls };

	if (insertObj.hs_student_participation === 'false') {
		insertObj.schoolreg_id = 906;
	}
	
	console.log(typeof insertObj.was_location_hs);
	if (insertObj.was_location_hs === 'true') {
		insertObj.location = insertObj.location_name_hs;
		const matches = /(.+)\s\((\d+)\)/g.exec(insertObj.location);
		if (matches) {
			const [
				,
				locationSchoolName,
				locationSchoolRegid,
			] = matches;
			insertObj.location_schoolreg_id = locationSchoolRegid;
			insertObj.location = locationSchoolName;
		}
	} else {
		insertObj.location = insertObj.location_name;
	}

	if (Array.isArray(insertObj.schoolreg_id)) {
		if (insertObj.schoolreg_id.length === 1) {
			insertObj.schoolreg_id = insertObj.schoolreg_id[0];
		} else {
			insertObj.multiple_schools = insertObj.schoolreg_id.join(',');
			insertObj.schoolreg_id = 905;
		}
	}
	return connection.query(`
	INSERT INTO witnessing_day
	(witnessing_time_spent, was_location_hs, was_debrief, training_time_spent, training_content, time_of_day, stories, spirit_filled_life, schoolreg_id, satisfied, recommittments, picture_url, others, multiple_schools, location, lifebooks, indicated_decisions, hs_student_witness, hs_student_names, gospels_of_john, gospel_presentations, gospel_booklets, follow_up_requests, debrief_explaination, date, csp_members, csp_member_names, conversation_with_christians, collected_data, chapter_id, bibles, approached, location_schoolreg_id)
	VALUES
	(:witnessing_time_spent, :was_location_hs, :was_debrief, :training_time_spent, :"training_content", :time_of_day, :stories, :spirit_filled_life, :schoolreg_id, :satisfied, :recommittments, :"picture_url", :others, :multiple_schools, :location, :lifebooks, :indicated_decisions, :hs_student_witness, :hs_student_names, :gospels_of_john, :gospel_presentations, :gospel_booklets, :follow_up_requests, :debrief_explaination, :date, :csp_members, :csp_member_names, :conversation_with_christians, :collected_data, :chapter_id, :bibles, :approached, :location_schoolreg_id)`,
	insertObj,
	returnSQLResponse(connection, response));
}
