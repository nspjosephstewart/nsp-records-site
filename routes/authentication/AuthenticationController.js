import {
	getSiteUrl,
	getHomeDirectory
} from '../util';

export const getLoginView = (request, response) =>
	response.sendFile('/client/dist/login.html', {root: getHomeDirectory() })

export const getFormsLoginView = (request, response) =>
	response.sendFile('/client/dist/login-forms.html', {root: getHomeDirectory()});

export const authenticateFormsWithGoogle = passport =>
	passport.authenticate('google', {
		scope:['profile', 'email'],
		projection: 'full',
	});

export const authenticateWithGoogle = passport =>
	passport.authenticate('google', {
		scope:["profile","email"],
		projection: "full"
	})

export const googleCallbackHook = passport => (request, response, next) =>
	passport.authenticate('google', {
		successRedirect : request.session.lastUrl || '/',
		failureRedirect : '/login',
	})(request, response, next)

export const logout = (request, response) => {
	request.logout();
	response.redirect(getSiteUrl() + "/login");
	response.end();
}

export const authenticateLocally = passport => 
	passport.authenticate('local-auth', {
		successRedirect : '/', // redirect to the secure profile section
		failureRedirect : '/login', // redirect back to the signup page if there is an erro
	});

export const getUser = (request,response) => {
	response.json(request.user);
}