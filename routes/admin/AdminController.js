import * as STATUS_CODES from 'http-status-codes';
import {
	returnSQLResponse,
	getPermissions,
} from '../util';

let google = require('googleapis');
let privatekey = require('../authentication/creds.json');

const mapUsers = user => {
	const permissions = (user.customSchemas && user.customSchemas.Records) || { has_access: false };
	let role;
	if (permissions.admin) {
		role = 'Admin';
	} else if (permissions.has_full_access) {
		role = 'Full Access';
	} else if (permissions.has_access) {
		role = 'User';
	} else {
		role = 'None';
	}
	return {
		email: user.primaryEmail,
		name: `${user.name.givenName} ${user.name.familyName}`,
		permissions: {
			...permissions,
			chapters: permissions.chapters && permissions.chapters.map(chapter => chapter.value),
			schools: permissions.schools && permissions.schools.map(school => school.value)
		},
		avatarUrl: user.thumbnailPhotoUrl,
		role,
		schoolsCount: permissions.schools ? permissions.schools.length : 0,
		chaptersCount: permissions.chapters ? permissions.chapters.length : 0
	};
}

export const listUsers = (request, response) => {

	const { isAdmin } = getPermissions(request);
	if (!isAdmin) {
		return response.status(STATUS_CODES.UNAUTHORIZED).send('You do not have admin status');
	}

	const jwtClient = new google.auth.JWT(
		privatekey.client_email,
		null,
		privatekey.private_key,
		['https://www.googleapis.com/auth/admin.directory.user'],
		"joseph.s@nationalschoolproject.com");

	var service = google.admin({ version: 'directory_v1', auth: jwtClient });
	service.users.list(
		{
			customer: 'my_customer',
			projection: 'full',
			maxResults: 500
		},
		(err, results) => {
			if (err) {
				return response.status(STATUS_CODES.INTERNAL_SERVER_ERROR).send('There was an error interacting with the Google API');
			}
			return response.json({
				users: results.users.map(mapUsers),
				nextPageToken: results.nextPageToken
			});
		}
	);
}

export const updatePermissions = (request, response) => {
	let { permissions, user, users } = request.body;
	const { isAdmin } = getPermissions(request);
	if (!isAdmin) {
		return response.status(STATUS_CODES.UNAUTHORIZED).json({ reason: 'You do not have admin status' });
	}

	if (user && users) {
		return response.status(STATUS_CODES.BAD_REQUEST).send({reason: 'Cannot define both user and users'});
	} else if (user) {
		users = [user];
	}

	if (permissions.chapters) {
		permissions.chapters = permissions.chapters.map(value => ({ type: "work", value }));
	}
	if (permissions.schools) {
		permissions.schools = permissions.schools.map(value => ({ type: "work", value }));
	}

	const jwtClient = new google.auth.JWT(
		privatekey.client_email,
		null,
		privatekey.private_key,
		['https://www.googleapis.com/auth/admin.directory.user'],
		"joseph.s@nationalschoolproject.com");

	const service = google.admin({ version: 'directory_v1', auth: jwtClient });

	try {
		const promises = users.map(userKey => {
			return new Promise((resolve, reject) => {
				service.users.update({
					userKey,
					resource: {
						customSchemas: {
							Records: permissions
						}
					},
				}, (err, results) => {
					if (err) {
						console.log(err);
						reject(err);
					}
					return resolve(results);
				});
			});
		});
		Promise.all(promises)
		.then(results => response.json(results))
		.catch(err => response.status(STATUS_CODES.INTERNAL_SERVER_ERROR).json({err}))
	} catch (err) {
		console.log(err);
		return response.status(STATUS_CODES.INTERNAL_SERVER_ERROR).json({err});
	}
}

export const updateWitnessingEvent = (request, response, connection) => {

	return response.status(STATUS_CODES.NOT_IMPLEMENTED).end();

	/*let { id } = request.params;

	if (!getPermissions(request).isAdmin) {
		response.status(STATUS_CODES.UNAUTHORIZED).send('You do not have admin status');
		return;
	} else if (isNaN(parseInt(id))) {
		response.status(STATUS_CODES.BAD_REQUEST).send('Improper id supplied');
		return;
	} else {

		let { changes } = request.body;
		let statements = getUpdateStatementFromChangeSet(changes);
		let sqlStat = `
			 UPDATE witnessing_day SET
			 ${statements} WHERE id = ${id}`;

		connection.query(
			sqlStat,
			returnSQLResponse(connection, response, () => ({ status: 200, reason: 'OK' }))
		)
	}*/

}

export const updateRallyEvent = (request, response, connection) => {

	return response.status(STATUS_CODES.NOT_IMPLEMENTED).end();

	/*let { id } = request.params;

	if (!getPermissions(request).isAdmin) {
		response.status(STATUS_CODES.UNAUTHORIZED).send('You do not have admin status');
		return;
	} else if (isNaN(parseInt(id))) {
		response.status(STATUS_CODES.BAD_REQUEST).send('Improper id supplied');
		return;
	} else {

		let { changes } = request.body;
		let statements = getUpdateStatementFromChangeSet(changes);
		let sqlStat = `
			UPDATE rallies SET
		 	${statements} WHERE id = ${id}`

		connection.query(
			sqlStat,
			returnSQLResponse(connection, response, () => ({ status: 200, reason: 'OK' })))
	}*/

}

export const getContactInfo = (request, response, connection) => {
	if (!request.user.isAdmin) {
		response.status(STATUS_CODES.UNAUTHORIZED).send('You do not have admin status');
	}

	let { id } = request.params

	connection.query(
		`SELECT speaker_email, speaker_phone, performer_phone, performer_email
    	FROM rallies
    	WHERE id = ${id}`,
		returnSQLResponse(connection, response)
	)
}
