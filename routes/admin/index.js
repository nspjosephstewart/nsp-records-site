import {
	isLoggedIn,
	withAdminDBConnection,
} from '../util';
import {
	updateWitnessingEvent,
	getContactInfo,
	updateRallyEvent,
	listUsers,
	updatePermissions,
} from './AdminController'

export default app => {
	/*app.post("/api/admin/witnessing/:id",
		isLoggedIn, updateWitnessingEvent);*/

	app.route('/api/admin/contactinfo/:id')
		.get(isLoggedIn, withAdminDBConnection(getContactInfo));

	app.get('/api/admin/rally/:id',
		isLoggedIn, updateRallyEvent);

	app.get('/api/admin/users/list',
		isLoggedIn, listUsers);

	app.route('/api/admin/users/update')
		.post(isLoggedIn, updatePermissions);
}
