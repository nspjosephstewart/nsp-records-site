import winston from 'winston';

const logger = winston.createLogger({
    format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.json()
    ),
    levels: {
        critical: 0,
        error: 1,
        warn: 2,
        info: 3
    },
    transports: [
        new winston.transports.File({
            filename: './logs/logs.log',
            level: 'info'
        }),
        new winston.transports.File({
            filename: './logs/errors.log',
            level: 'error',
        })
    ]
});

export default logger;