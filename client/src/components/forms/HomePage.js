import React from 'react';

import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';

import { get } from '../../utils/fetch';

import styles from './home.less';

export default class HomePage extends React.Component {
	state = {
		forms: [],
	}

	componentDidMount = async () => {
		const forms = await get('/api/forms');
		this.setState({ forms });
	}

	mapFormCard = form => (
		<Card className={styles.card}>
			<CardActionArea href={`/forms/${form.name}`}>
				{form.thumbnail_url ?
					<CardMedia
						image={form.thumbnail_url}
						title={form.title}
						className={styles.cardImage}
					/>
					:
					null
				}
				<Typography variant="h3" className={styles.cardTitle}>
					{form.title}
				</Typography>
			</CardActionArea>
		</Card>
	)

	render() {
		const { forms } = this.state;

		return (
			<div className={styles.root}>
				<Typography variant="h1" className={styles.title}>Campus Records</Typography>
				<div className={styles.options}>
					{forms.map(this.mapFormCard)}
				</div>
			</div>
		)
	}

}