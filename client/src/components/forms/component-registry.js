import React from 'react';
import FileUpload from '../elements/form-components/FileUpload';
import TextField from '@material-ui/core/TextField';
import ChapterSelect from '../elements/form-components/ChapterSelect';
import Bool from '../elements/form-components/Boolean';
import SchoolSelect from '../elements/form-components/SchoolSelect';
import Radio from '../elements/form-components/Radio';
import Checkbox from '../elements/form-components/Checkbox';

export const components = {
	'text': props => (<TextField {...props} onChange={event => props.onChange(event.target.value)} />),
	'long_text': props => (<TextField multiline {...props} onChange={event => props.onChange(event.target.value)} />),
	'number': props => (<TextField type="number" {...props} onChange={event => props.onChange(event.target.value)} />),
	'file': FileUpload,
	'chapter_select': ChapterSelect,
	'boolean': Bool,
	'school_select': SchoolSelect,
	'date': props => (<TextField {...props} type="date" onChange={event => props.onChange(event.target.value)} value={props.value} />),
	'radio': Radio,
	'checkbox': Checkbox,
}