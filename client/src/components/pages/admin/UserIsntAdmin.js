import React from 'react';

export default class UserIsntAdmin extends React.Component {
	render() {
		return (
			<p>You aren't an admin!</p>
		)
	}
}