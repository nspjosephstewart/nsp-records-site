import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Switch from '@material-ui/core/Switch';
import Fab from '@material-ui/core/Fab';
import PagePanel from './PagePanel';

import Editor from './Editor';

import { get, post } from '../../../../utils/fetch';

import styles from './forms.less';

export default class EditFormPage extends React.Component {
	state = {
		loading: true,
		saving: false,
		title: '',
		name: '',
		subtitle: '',
		customHandler: '',
		pages: [],
		active: false,
		unsavedChanges: false,
		selectedPanel: -1,
		active: false,
	}
	
	componentDidMount() {
		this.loadFormPage();
	}

	loadFormPage = async () => {
		const { formId } = this.props;
		const form = await get(`/api/admin/forms/${formId}`);
		if (!form.pages) return;

		this.setState({
			name: form.name,
			title: form.title,
			subtitle: form.subtitle,
			active: form.active,
			pages: JSON.parse(form.pages).map((page, id) => {
				return {
					...page,
					id,
					questions: page.questions.map((obj, id) => ({ ...obj, id }))
				}
			}),
			loading: false,
			unsavedChanges: false,
		})
	}

	updateFormPage = () => {
		this.setState({
			unsavedChanges: false,
			saving: true,
		}, async () => {
			const { formId } = this.props;
			const {
				active,
				pages,
				title,
				subtitle,
				name,
			} = this.state
			await post(`/api/admin/forms/${formId}`, {
				active,
				pages,
				title,
				subtitle,
				name,
			});
			this.setState({
				saving: false
			});
		});
	}

	handleChange = name => value => {
		this.setState({
			[name]: value,
			unsavedChanges: true,
		});
	}

	handleQuestionChange = (pageId, questionId, attribute) => value => {
		this.setState(prevState => {
			if (
				!prevState.pages ||
				!prevState.pages[pageId] || 
				!prevState.pages[pageId].questions ||
				!prevState.pages[pageId].questions[questionId]
			) {
				return;
			}

			prevState.pages[pageId].questions[questionId][attribute] = value;

			return {
				unsavedChanges: true,
				pages: prevState.pages
			}
		})
	}

	handlePageChange = (pageId, attribute) => value => {
		this.setState(prevState => {
			if (
				!prevState.pages ||
				!prevState.pages[pageId] ||
				attribute === 'questions'
			) {
				return;
			}

			prevState.pages[pageId][attribute] = value;

			return {
				unsavedChanges: true,
				pages: prevState.pages,
			}
		})
	}

	handlePanelExpanded = id => (event, expanded) => {
		this.setState({
			selectedPanel: expanded ? id : -1
		})
	}

	findMaxQuestionId = (pages, pageId) => {
		const id = Math.max(...pages[pageId].questions.map(question => question.id)) + 1;

		if (id && id > 0) return id;
		return 0;
	}

	findMaxPageId = pages => {
		const id = Math.max(...pages.map(page => page.id)) + 1;

		if (id && id > 0) return id;
		return 0;
	}

	handleAddQuestion = pageId => {
		this.setState(({ pages }) => {
			const pagesCopy = JSON.parse(JSON.stringify(pages));
			pagesCopy[pageId].questions.push({
				id: this.findMaxQuestionId(pages, pageId) || 0,
				type: 'text'
			});

			return {
				pages: pagesCopy,
				unsavedChanges: true,
			};			
		});
	}

	handleRemoveQuestion = (pageId, questionId) => {
		this.setState(({ pages }) => {
			const pagesCopy = JSON.parse(JSON.stringify(pages));
			pagesCopy[pageId].questions.splice(questionId, 1);
			return {
				pages: pagesCopy,
				unsavedChanges: true,
			}
		})
	}

	handleMoveQuestionUp = (pageId, questionId) => {
		if (questionId === 0) {
			return;
		}
		
		this.setState(({ pages }) => {
			const pagesCopy = JSON.parse(JSON.stringify(pages));
			const temp = pagesCopy[pageId].questions[questionId - 1];
			pagesCopy[pageId].questions[questionId - 1] = pagesCopy[pageId].questions[questionId];
			pagesCopy[pageId].questions[questionId] = temp;
			return {
				pages: pagesCopy,
				unsavedChanges: true,
			}
		})
	}

	handleMoveQuestionDown = (pageId, questionId) => {
		if (questionId >= this.state.pages[pageId].questions.length - 1) {
			return;
		}

		this.setState(({ pages }) => {
			const pagesCopy = JSON.parse(JSON.stringify(pages));
			const temp = pagesCopy[pageId].questions[questionId + 1];
			pagesCopy[pageId].questions[questionId + 1] = pagesCopy[pageId].questions[questionId];
			pagesCopy[pageId].questions[questionId] = temp;
			return {
				pages: pagesCopy,
				unsavedChanges: true,
			}
		})
	}

	handleMovePageUp = pageId => {
		if (pageId === 0) {
			return;
		}

		this.setState(({ pages }) => {
			const pagesCopy = JSON.parse(JSON.stringify(pages));
			const temp = pagesCopy[pageId - 1];
			pagesCopy[pageId - 1] = pagesCopy[pageId];
			pagesCopy[pageId] = temp;
			return {
				pages: pagesCopy,
				unsavedChanges: true,
			}
		})
	}

	handleMovePageDown = pageId => {
		if (pageId >= this.state.pages.length - 1) {
			return;
		}

		this.setState(({ pages }) => {
			const pagesCopy = JSON.parse(JSON.stringify(pages));
			const temp = pagesCopy[pageId + 1];
			pagesCopy[pageId + 1] = pagesCopy[pageId];
			pagesCopy[pageId] = temp;
			return {
				pages: pagesCopy,
				unsavedChanges: true,
			}
		})
	}

	handleDeletePage = pageId => {
		this.setState(({ pages }) => {
			const pagesCopy = JSON.parse(JSON.stringify(pages));
			pagesCopy.splice(pageId, 1);
			return {
				pages: pagesCopy,
				unsavedChanges: true,
			}
		})
	}
	
	handleAddPage = () => {
		this.setState(({ pages }) => {
			const pagesCopy = JSON.parse(JSON.stringify(pages));
			pagesCopy.push({
				questions: [],
				id: this.findMaxPageId(pages) || 0,
			});
			return {
				pages: pagesCopy,
				unsavedChanges: true
			}
		});
	}

	render() {
		const {
			loading,
			name,
			title,
			subtitle,
			unsavedChanges,
			pages,
			selectedPanel,
			active,
			saving,
		} = this.state;
		const { onBack } = this.props;
		
		if (loading) {
			return (
				<div className={styles.spinnerContainer}>
					<CircularProgress />
				</div>
			)
		}

		return (
			<Paper className={styles.root}>
				<div className={styles.editPageHeader}>
					<div className={styles.titleContainer}>
						<Typography variant="h3" className={styles.title}>
							<Editor
								value={title}
								onChange={this.handleChange('title')}
								placeholder="Title..."
							/>
						</Typography>
						<Typography variant="h5" className={styles.name}>
							<Editor
								value={name}
								onChange={this.handleChange('name')}
								placeholder="[name]"
								rightAdornment={<span>records.nationalschoolproject.com/forms/</span>}
							/>
						</Typography>
					</div>
					<div className={styles.spacer} />
					<IconButton onClick={onBack}>
						<Icon>arrow_back</Icon>
					</IconButton>
					<IconButton onClick={this.loadFormPage}>
						<Icon>refresh</Icon>
					</IconButton>
					{unsavedChanges ?
						<IconButton onClick={this.updateFormPage}>
							<Icon>save</Icon>
						</IconButton>
					: saving ?
						<CircularProgress size={20} />
					:
						null
					}
				</div>
				<Divider className={styles.divider} />
				<Typography variant="h6" className={styles.label}>
				Subtitle:
				</Typography>
				<Editor
					value={subtitle}
					onChange={this.handleChange('subtitle')}
					placeholder="Subtitle..."
					multiline
				/>
				<div className={`${styles.row} ${styles.spacingSmall}`}>
					<Typography variant="h6" className={styles.label}>Active: </Typography>
					<Switch checked={active} onChange={(event, checked) => this.handleChange('active')(checked)} />
				</div>
				<Typography variant="h6" className={`${styles.spacing} ${styles.label}`}>
				Pages
				</Typography>
				<PagePanel
					pages={pages}
					selectedPanel={selectedPanel}
					handlePanelExpanded={this.handlePanelExpanded}
					handleQuestionChange={this.handleQuestionChange}
					handleAddQuestion={this.handleAddQuestion}
					handleRemoveQuestion={this.handleRemoveQuestion}
					handleMoveQuestionUp={this.handleMoveQuestionUp}
					handleMoveQuestionDown={this.handleMoveQuestionDown}
					handleMovePageUp={this.handleMovePageUp}
					handleMovePageDown={this.handleMovePageDown}
					handleDeletePage={this.handleDeletePage}
					handlePageChange={this.handlePageChange}
				/>
				<div className={`${styles.spacingSmall} ${styles.center}`}>
					<Fab
						variant="extended"
						size="small"
						color="primary"
						onClick={this.handleAddPage}
					>
						<Icon style={{marginRight: 5}}>add</Icon>
						<Typography
							variant="button"
							color="inherit"
							style={{marginRight: 8}}
						>
							Add Page
						</Typography>
					</Fab>
				</div>
			</Paper>
		)
	}
}