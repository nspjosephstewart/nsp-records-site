import React from 'react';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import TextField from '@material-ui/core/TextField';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Icon from '@material-ui/core/Icon';
import QuestionComponent from './QuestionComponent';

import styles from './forms.less';

export default class PagePanel extends React.Component {
	mapQuestions = pageId => (question, index, arr) => {
		const { handleQuestionChange, handleRemoveQuestion, handleMoveQuestionUp, handleMoveQuestionDown } = this.props;
		
		return (
			<QuestionComponent
				key={question.id}
				title={question.title}
				name={question.name}
				required={question.required}
				type={question.type}
				condition={question.condition}
				subtitle={question.subtitle}
				options={question.options}
				handleQuestionChange={handleQuestionChange}
				pageId={pageId}
				questionId={index}
				handleRemoveQuestion={handleRemoveQuestion}
				handleMoveQuestionDown={handleMoveQuestionDown}
				handleMoveQuestionUp={handleMoveQuestionUp}
				questionCount={arr.length}
			/>
		)
	}

	hasQuestionBeenAddedOrMoved = (nextPages, pages) => {
		for (let i = 0; i < nextPages.length; i++) {
			if (nextPages[i].questions.length !== pages[i].questions.length) {
				return true;
			}
			const nextQuestions = nextPages[i].questions;
			const questions = pages[i].questions;
			for (let j = 0; j < nextQuestions.length; j++) {
				if (nextQuestions[j].id !== questions[j].id) {
					return true;
				}
			}
		}
		return false;
	}

	shouldComponentUpdate(nextProps) {
		if (
			nextProps.selectedPanel !== this.props.selectedPanel ||
			nextProps.pages.length !== this.props.pages.length ||
			this.hasQuestionBeenAddedOrMoved(nextProps.pages, this.props.pages)
		) {
			return true;
		}
		return false;
	}
	
	mapPages = (page, index, arr) => {
		const {
			selectedPanel,
			handlePanelExpanded,
			handleAddQuestion,
			handleDeletePage,
			handlePageChange,
			handleMovePageUp,
			handleMovePageDown,
		} = this.props;
		return (
			<ExpansionPanel
				onChange={handlePanelExpanded(page.id)}
				expanded={selectedPanel === page.id}
				key={page.id}
				elevation={0}
			>
				<ExpansionPanelSummary expandIcon={<Icon>expand_more</Icon>}>
					<Typography className={styles.panelHeading}>Page {index+1}</Typography>
				</ExpansionPanelSummary>
				<ExpansionPanelDetails className={styles.panelDetails}>
					<div className={styles.row}>
						<Typography variant="h3" style={{flexGrow: 1}}>
							<TextField
								fullWidth
								InputProps={{
									style: {
										fontSize: 'inherit'
									}
								}}
								placeholder="Page Title..."
								onChange={event => handlePageChange(index, 'title')(event.target.value)}
								defaultValue={page.title}
							/>
						</Typography>
						<IconButton disabled={index === 0} onClick={() => handleMovePageUp(index)}>
							<Icon fontSize="small">arrow_upward</Icon>
						</IconButton>
						<IconButton disabled={index === arr.length - 1} onClick={() => handleMovePageDown(index)}>
							<Icon fontSize="small">arrow_downward</Icon>
						</IconButton>
						<IconButton onClick={() => handleDeletePage(index)}>
							<Icon fontSize="small">delete</Icon>
						</IconButton>
					</div>
					<div className={styles.spacingBig}>
						{page.questions && page.questions.length > 0 ?
							page.questions.map(this.mapQuestions(index))
						:
							<Typography variant="display1" style={{textAlign: 'center'}}>No Questions</Typography>
						}
					</div>
					<div className={`${styles.spacing} ${styles.center}`}>
						<Button onClick={() => handleAddQuestion(index)}>
							<Icon>add</Icon>
							<Typography variant="button">Add Question</Typography>
						</Button>
					</div>
				</ExpansionPanelDetails>
			</ExpansionPanel>
		)
	}

	render() {
		const { pages } = this.props;
		return (
			<React.Fragment>
				{pages.map(this.mapPages)}
			</React.Fragment>
		)
	}
}