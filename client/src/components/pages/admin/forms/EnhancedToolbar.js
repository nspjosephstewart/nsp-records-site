import React from 'react';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import AppBarInput from '../../../elements/app-bar-input/AppBarInput';

import styles from '../styles.less';

export default class EnhancedToolbar extends React.Component {
	state = {
		filterShowing: false,
	}

	render() {
		const { anchor } = this.state;
		const {
			refresh,
			search,
			searchValue,
			newForm
		} = this.props;

		return (
			<Toolbar
				className={styles.toolbar}
			>
				<div className={styles.toolbarRow}>
					<Typography variant="h5">Forms</Typography>
					<div className={styles.spacer} />
					<AppBarInput
						onChange={search}
						value={searchValue}
						placeholder="Search..."
						icon={(
							<Icon>search</Icon>
						)}
					/>
					<IconButton onClick={refresh}>
						<Icon>refresh</Icon>
					</IconButton>
					<IconButton onClick={newForm}>
						<Icon>add</Icon>
					</IconButton>
				</div>
			</Toolbar>
		)
	}
}