import React from 'react';
import AutosizeInput from 'react-input-autosize';

import styles from './forms.less';

export default class Editor extends React.Component {
	state = {
		editing: false,
	}

	handleKeyPress = event => {
		if (event.key === 'Enter') {
			this.setState({ editing: false });
		}
	}
	
	render() {
		const { editing } = this.state;
		const {
			value,
			onChange,
			placeholder,
			rightAdornment,
			leftAdornment,
			multiline,
		} = this.props;

		const inputStyle = {
			fontSize: 'inherit',
			fontWeight: 'inherit',
			fontFamily: 'inherit',
			lineHeight: 'inherit',
			color: 'inherit',
			minWidth: rightAdornment ? undefined : 100,
			outline: 'none',
			background: 'none',
			border: 'none',
		}

		if (!editing) {
			return (
				<div
					className={styles.editor}
					onClick={() => this.setState({ editing: true })}
					style={{
						width: multiline ? '100%' : undefined,
						display: multiline ? 'block' : undefined
					}}
				>
					{
						rightAdornment ?
							rightAdornment
						:
							null
					}
					{value || <span className={styles.editorPlaceholder}>{placeholder || 'Edit me'}</span>}
					{
						leftAdornment ?
							leftAdornment
						:
							null
					}
				</div>
			);
		}

		if (multiline) {
			return (
				<textarea
					autoFocus
					style={{
						width: '100%',
					}}
					onBlur={() => this.setState({editing: false})}
					placeholder={placeholder || 'Edit me'}
					onChange={event => onChange(event.target.value)}
					value={value}
					className={styles.editorInput}
					rows={3}
				/>
			)
		}

		return (
			<div className={styles.editorInput}>
				{
					rightAdornment ?
						rightAdornment
					:
						null
				}
				<AutosizeInput
					value={value}
					onChange={event => onChange(event.target.value)}
					autoFocus={true}
					style={{
						maxWidth: '100%'
					}}
					inputStyle={inputStyle}
					placeholder={placeholder || 'Edit me'}
					onKeyPress={this.handleKeyPress}
					onBlur={() => this.setState({ editing: false })}
				/>
				{
					leftAdornment ?
						leftAdornment
					:
						null
				}
			</div>
		);
	}
}