import React from 'react';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Switch from '@material-ui/core/Switch';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';

import TypeAndConditionsEditor from './TypeAndConditionalEditor';

import styles from './forms.less';

export default class QuestionComponent extends React.Component {
	render() {
		const {
			title,
			name,
			required,
			type,
			condition,
			subtitle,
			handleQuestionChange,
			pageId,
			questionId,
			handleRemoveQuestion,
			handleMoveQuestionUp,
			handleMoveQuestionDown,
			questionCount,
			options,
		} = this.props;

		return (
			<div className={styles.question}>
				<div className={styles.row}>
					<Typography variant="h4" style={{flexGrow: 1}}>
						<TextField
							defaultValue={title}
							onChange={event => handleQuestionChange(pageId, questionId, 'title')(event.target.value)}
							fullWidth
							InputProps={{
								style: {
									fontSize: 'inherit'
								}
							}}
						/>
					</Typography>
					<IconButton onClick={() => handleMoveQuestionUp(pageId, questionId)} disabled={questionId === 0}>
						<Icon fontSize="small">arrow_upward</Icon>
					</IconButton>
					<IconButton onClick={() => handleMoveQuestionDown(pageId, questionId)} disabled={questionId === questionCount - 1}>
						<Icon fontSize="small">arrow_downward</Icon>
					</IconButton>
					<IconButton onClick={() => handleRemoveQuestion(pageId, questionId)}>
						<Icon fontSize="small">delete</Icon>
					</IconButton>
				</div>
				<div className={`${styles.questionName} ${styles.row}`}>
					<Typography variant="inherit" className={styles.rowLabel}>
						Identifier:&nbsp;
					</Typography>
					<TextField
						defaultValue={name}
						onChange={event => handleQuestionChange(pageId, questionId, 'name')(event.target.value)}
						className={styles.rowInput}
					/>
				</div>
				<Typography className={`${styles.rowLabel} ${styles.spacingSmall}`}>Subtitle</Typography>
				<TextField
					onChange={event => handleQuestionChange(pageId, questionId, 'subtitle')(event.target.value)}
					fullWidth
					multiline
					rows={3}
					defaultValue={subtitle}
				/>
				<div className={`${styles.spacingSmall} ${styles.row}`}>
					<Typography variant="inherit" className={styles.rowLabel}>
						Required:
					</Typography>
					<Switch
						defaultChecked={Boolean(required)}
						onChange={(event, checked) => handleQuestionChange(pageId, questionId, 'required')(checked)}
					/>
				</div>
				<TypeAndConditionsEditor
					type={type}
					condition={condition}
					pageId={pageId}
					questionId={questionId}
					onChange={handleQuestionChange}
					choices={options}
				/>
			</div>
		)
	}
}