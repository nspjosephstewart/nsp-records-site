import React from 'react';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Collapse from '@material-ui/core/Collapse';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';

import { components } from '../../../forms/component-registry';
const componentTypes = Object.keys(components);

import styles from './forms.less';

export default class TypeEditor extends React.Component {
	state = {
		expanded: false,
		type: '',
	}

	componentDidMount() {
		const { type } = this.props;
		this.setState({
			type
		});
	}
	
	mapTypes = (type, index) => (
		<MenuItem value={type} key={index}>
			{type}
		</MenuItem>
	)

	handleTypeChange = event => {
		const { pageId, questionId, onChange } = this.props;
		this.setState({
			type: event.target.value
		}, () => {
			onChange(pageId, questionId, 'type')(this.state.type);
		});
	}

	handleOptionsChange = event => {
		const { pageId, questionId, onChange } = this.props;
		const { type } = this.state;
		const typeName = type.split(':')[0];
		const newType = `${typeName}:${event.target.value}`;
		this.setState({type: newType}, () => {
			onChange(pageId, questionId, 'type')(this.state.type);
		});
	}
	
	render() {
		const { condition, pageId, questionId, onChange, choices } = this.props;
		const { expanded, type } = this.state;

		const typeSplit = type.split(':');
		const typeName = typeSplit.shift();
		const optionsText = typeSplit.join(':');

		return (
			<div className={styles.typeEditor}>
				<div className={styles.row}>
					<Typography className={styles.rowLabel} variant="inherit">Type: </Typography>
					<Select
						value={typeName}
						className={styles.rowInput}
						onChange={this.handleTypeChange}
					>
						{componentTypes.map(this.mapTypes)}
					</Select>
				</div>
				<div className={styles.center}>
					<Button onClick={() => this.setState(({expanded}) => ({expanded: !expanded}))}>
						{expanded ? 'Less' : 'More'}
					</Button>
				</div>
				<Collapse in={expanded}>
					<div className={`${styles.spacingSmall} ${styles.row}`}>
						<Typography variant="inherit" className={styles.rowLabel}>
							Choices (comma separated):
						</Typography>
						<TextField
							defaultValue={choices && choices.join(', ')}
							onChange={event => onChange(pageId, questionId, 'options')(event.target.value.split(/,\s+/))}
						/>
					</div>
					<div className={`${styles.spacingSmall} ${styles.row}`}>
						<Typography variant="inherit" className={styles.rowLabel}>Type Options: </Typography>
						<TextField
							className={styles.rowInput}
							defaultValue={optionsText}
							onChange={this.handleOptionsChange}
						/>
					</div>
					<div className={`${styles.spacingSmall} ${styles.row}`}>
						<Typography variant="inherit" className={styles.rowLabel}>Display Condition: </Typography>
						<TextField
							className={styles.rowInput}
							defaultValue={condition}
							onChange={event => onChange(pageId, questionId, 'condition')(event.target.value)}
						/>
					</div>
				</Collapse>
			</div>
		)
	}
}