import React from 'react';

import Checkbox from '@material-ui/core/Checkbox';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';
import Typography from '@material-ui/core/Typography';

import EnhancedToolbar from './EnhancedToolbar';
import EditFormPage from './EditFormPage';
import PageTable from '../../../elements/page-table/PageTable';

import { get, post } from '../../../../utils/fetch';

export default class FormsPage extends React.Component {
	state = {
		searchValue: '',
		updating: false,
		editingForm: null,
		newFormModalOpen: false,
		newTitle: '',
		newNameValue: '',
	}

	filterForms = searchValue => form => (
				!searchValue || 
				form.title.includes(searchValue) || 
				form.title.includes(searchValue)
			)

	toggleFormActive = id => event => {
		const { refresh } = this.props;
		let method;
		if (event.target.checked) {
			method = 'activate'
		} else {
			method = 'deactivate'
		}

		this.setState({
			updating: true
		}, async () => {
			await get(`/api/admin/forms/${id}/${method}`);
			refresh(() => {
				this.setState({ updating: false });
			});
		})
	}

	newForm = () => {
		this.setState({
			newFormModalOpen: false,
			updating: true,
		}, async () => {
			const { newNameValue } = this.state;
			const result = await post('/api/admin/forms', {
				name: newNameValue,
			});
			this.setState({
				updating: false,
				editingForm: result.id,
			});
		})
	}

	render() {
		const { searchValue, updating, editingForm, newFormModalOpen, newNameValue } = this.state;
		const { forms, loading, refresh } = this.props;

		if (typeof editingForm === 'number') {
			return (
				<EditFormPage
					formId={editingForm}
					onBack={() => this.setState({ editingForm: null })}
				/>
			)
		}

		return (
			<React.Fragment>
				<PageTable
					head={{
						'title': {
							name: 'Title'
						},
						'creator': {
							name: 'Support Person',
						},
						'name': {
							name: 'Endpoint',
							getCell: value => {
								return (
									<span>/forms/{value}</span>
								)
							}
						},
						'active': {
							name: 'Active',
							getCell: (value, row) => {
								return (
									<Checkbox checked={value} onChange={this.toggleFormActive(row.id)}/>
								)
							},
							checkbox: true,
						}
					}}
					onRowClicked={editingForm => this.setState({ editingForm: editingForm.id })}
					loading={loading || updating}
					data={forms.filter(this.filterForms(searchValue))}
					noMargin
					Toolbar={
						<EnhancedToolbar
							searchValue={searchValue}
							search={event => this.setState({ searchValue: event.target.value })}
							refresh={refresh}
							newForm={() => this.setState({ newFormModalOpen: true })}
						/>
					}
				/>
				<Dialog
					open={newFormModalOpen}
					onClose={() => this.setState({ newFormModalOpen: false })}
					fullWidth
					maxWidth="sm"
				>
					<DialogTitle>New Form</DialogTitle>
					<DialogContent>
						<Typography variant="h6">Please enter a title:</Typography>
						<Input
							fullWidth
							value={newNameValue}
							onChange={event => this.setState({ newNameValue: event.target.value })}
						/>
					</DialogContent>
					<DialogActions>
						<Button
							onClick={() => this.setState({ newFormModalOpen: false })}
						>
							Cancel
						</Button>
						<Button
							color="primary"
							onClick={this.newForm}
						>
							Create
						</Button>
					</DialogActions>
				</Dialog>
			</React.Fragment>
		)
	}
}