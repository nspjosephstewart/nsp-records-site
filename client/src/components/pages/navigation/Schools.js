import React, {Component} from 'react';
import {Link} from 'react-router-dom'
import 'whatwg-fetch';
import _ from 'lodash';
import PageTable from '../../elements/page-table/PageTable';
import SchoolEnrollmentForm from '../../elements/school-enrollment/SchoolEnrollmentForm';
import IfUserIsAdmin from '../../elements/utility/if-user-is-admin/IfUserIsAdmin';
import AppBarInput from '../../elements/app-bar-input/AppBarInput';
import { get } from '../../../utils/fetch';
import { bindKeys, unbindKeys } from '../../../utils/hotkeys';
import MobileDisplayOnly from '../../elements/utility/displays/MobileDisplayOnly';
import DesktopDisplayOnly from '../../elements/utility/displays/DesktopDisplayOnly';

import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Checkbox from '@material-ui/core/Checkbox';
import Radio from '@material-ui/core/Radio';
import Divider from '@material-ui/core/Divider';
import Tooltip from '@material-ui/core/Tooltip';

import {
	getCurrentYearOfMinistry
} from '../../../utils/date_calculations';

import styles from './schools.css';

class Schools extends Component {
	constructor(props) {
		super(props);

		let current_ministry_year = getCurrentYearOfMinistry('MM/DD/YYYY');

		this.state = {
			loading: true,
			schools: [],
			filteredSchools: [],
			viewType: "grid",
			maxDate: current_ministry_year.max_date,
			minDate: current_ministry_year.min_date,
			filterRegex:"name",
			modalIsOpen: false,
			enrolledSchoolsOnly: true,
			anchorElement: null,
		};
	}

	hotKeys = {
		'ctrl+f': event => {
			event.preventDefault();
			if (this.search_field) this.search_field.focus()
		}
	}

	handleSchoolsChange(schools) {
		if (!this.search_field) return;
		this.search_field.value = "";
		return this.setState({
			schools: schools,
			filteredSchools:schools,
			loading: false,
		});
	}

	componentDidMount() {
		this.fetchSchools();
		bindKeys(this.hotKeys);
	}

	componentWillUnmount() {
		unbindKeys(this.hotKeys);
	}

	fetchSchools = () => {
		const { enrolledSchoolsOnly } = this.state;
		const enrolledSchoolsOnlyString = enrolledSchoolsOnly ? '&enrolledSchoolsOnly=true' : '';
		this.setState( { loading: true },
			() => get(`/api/schools?${enrolledSchoolsOnlyString}`)
				.then(response => {
					this.handleSchoolsChange(response);
				})
		)
	}

	handleToggleFor = name => () => {
		this.setState(state => ({
			[name]: !state[name],
		}), this.fetchSchools)
	}

	filterSchools() {
		if (!this.search_field) return
		let value = this.search_field.value;
		this.setState({
			filteredSchools: _.filter(this.state.schools,(school) => {
				if (this.state.filterRegex != null && school[`${this.state.filterRegex}`] != null)
					return school[`${this.state.filterRegex}`].toString().toLowerCase().indexOf(value.toLowerCase()) !== -1;
				else
					return false;
			})
		})
	}

	handleGridClick() {
		this.setState({
			viewType:"grid",
			filterRegex:"name"
		})
	}

	handleListClick() {
		this.setState({
			viewType:"list"
		})
	}

	changeFilter(filterRegex) {
		if (!this.search_field) return;
		this.search_field.value = "";
		this.setState({
			filterRegex
		})
	}

	openModal = () => {
		this.setState({
			modalIsOpen: true,
			anchorElement: null,
		});
	}

	closeModal = () => {
		this.setState({
			modalIsOpen: false,
		})
	}

	openMenu = event => {
		this.setState({ anchorElement: event.currentTarget });
	}

	closeMenu = () => {
		this.setState({ anchorElement: null });
	}

	render() {
		const {
			enrolledSchoolsOnly,
			anchorElement,
			minDate,
			maxDate,
			filteredSchools,
			modalIsOpen
		} = this.state;

		return (
			<div>
				<AppBar color="default" position="relative" elevation={1}>
					<Toolbar>
						<DesktopDisplayOnly className={styles.grow}>
							<Typography variant="h6" color="inherit">Schools ({filteredSchools.length})</Typography>
						</DesktopDisplayOnly>

						<AppBarInput
							onChange={this.filterSchools.bind(this)}
							placeholder="Search..."
							inputRef={ref => this.search_field = ref}
							icon={(
								<Icon>search</Icon>
							)}
						/>
						<DesktopDisplayOnly className={styles.headerBarItem}>
							<IconButton
								className={styles.headerBarItem}
								onClick={this.handleGridClick.bind(this)}
								color={this.state.viewType==='grid' ? 'secondary' : undefined}
							>
								<Icon>view_module</Icon>
							</IconButton>
							<IconButton
								className={styles.headerBarItem}
								onClick={this.handleListClick.bind(this)}
								color={this.state.viewType==='list' ? 'secondary' : undefined}
							>
								<Icon>view_list</Icon>
							</IconButton>
						</DesktopDisplayOnly>
						<IconButton
							onClick={this.openMenu}
						>
							<Icon>more_vert</Icon>
						</IconButton>
						<Menu
							anchorEl={anchorElement}
							open={Boolean(anchorElement)}
							onClose={this.closeMenu}
						>
							<Tooltip title={`Current ministry year: ${minDate} - ${maxDate}`} enterDelay={500}>
								<MenuItem
									onClick={this.handleToggleFor('enrolledSchoolsOnly')}
									className={styles.menuItem}
								>
										<Typography
											variant="button"
										>
											Show currently enrolled schools only
										</Typography>
										<Checkbox
											disableRipple
											tabIndex={-1}
											checked={enrolledSchoolsOnly}
										/>
								</MenuItem>
							</Tooltip>
							<MobileDisplayOnly>
								<Divider/>
								<MenuItem
									onClick={this.handleListClick.bind(this)}
									className={styles.menuItem}
								>
									<Typography
										variant="button"
									>
										View List
									</Typography>
									<Radio
										disableRipple
										tabIndex={-1}
										checked={this.state.viewType==='list'}
									/>
								</MenuItem>
								<MenuItem
									onClick={this.handleGridClick.bind(this)}
									className={styles.menuItem}
								>
									<Typography
										variant="button"
									>
										View Grid
									</Typography>
									<Radio
										disableRipple
										tabIndex={-1}
										checked={this.state.viewType==='grid'}
									/>
								</MenuItem>
							</MobileDisplayOnly>
							<IfUserIsAdmin>
								<Divider />
								<MenuItem
									onClick={this.openModal}
								>
									<Typography variant="button">
										Register School(s)
									</Typography>
								</MenuItem>
							</IfUserIsAdmin>
						</Menu>
					</Toolbar>
				</AppBar>

				{this.state.loading ? <div className="mdl-spinner mdl-js-spinner is-active"></div> : ''}

				{
					this.state.viewType == "grid" ?
						<ul className="mdl-grid mdl-list mdl-cell mdl-cell--12-col card-container">
							{
								filteredSchools.map(
									school => {
										return <li className="mdl-cell mdl-list__item" key={school.id}>
											<Link
												to={`/school/${school.id}`}
											>
												<button className="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect">
													{school.name}
												</button>
											</Link>
										</li>
									}
								)
							}
						</ul>
					: this.state.viewType == "list" ?
						<PageTable
							cssClass="mdl-data-table mdl-js-data-table mdl-cell mdl-cell--12-col"
							data={filteredSchools}
							head={
								{
									'name': {
										name: 'Name',
										onClick: () => this.changeFilter('name'),
									},
									'address': {
										name: 'Address',
										onClick: () => this.changeFilter('address'),
										hideBreakpoint: 'sm',
									},
									'zip': {
										name: 'Zip',
										onClick: () => this.changeFilter('zip'),
										hideBreakpoint: 'sm',
									},
									'state': {
										name: 'State',
										onClick: () => this.changeFilter('state'),
									},
									'chapter': {
										name: 'Most Recent Chapter',
										onClick: () => this.changeFilter('chapter'),
										getCell: (value, row) => (
											<Link to={`/chapters/${row.chapter_id}`}>{value}</Link>
										)
									},
									'id': {
										getCell: value => (
											<Link to={`/school/${value}`}>
												<i className="material-icons">add</i>
											</Link>
										)
									}
								}
							}
						/>
					: ''
				}
				<SchoolEnrollmentForm
					open={modalIsOpen}
					onClose={this.closeModal}
				/>
			</div>
		)
	}
}

export default Schools;
