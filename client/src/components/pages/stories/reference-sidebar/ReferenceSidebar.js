import React from 'react';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import ReferenceTimeline from '../reference-timeline/ReferenceTimeline';

import { get, httpDelete, post } from '../../../../utils/fetch';

import styles from './styles.less';

export default class ReferenceSidebar extends React.Component {
	state = {
		references: []
	}
	
	componentDidMount() {
		this.fetchReferences();
	}

	fetchReferences = async () => {
		const { storyId } = this.props;
		const references = await get(`/api/stories/${storyId}/references`);
		this.setState({
			references,
		});
	}

	getSource = source => {
		const { sourceId } = this.props;

		switch(source) {
			case 'witnessing':
				return (
					<a href={`/witnessingday/${sourceId}`} target="_blank">Witnessing Event</a>
				);
			case 'rallies':
				return (
					<a href={`/rally/${sourceId}`} target="_blank">Rally</a>
				);
			case 'story':
				return 'Story Capture';
			default:
				return <em>NA</em>
		}
	}

	deleteReference = async outletId => {
		const { storyId } = this.props;
		await httpDelete(`/api/stories/${storyId}/references/${outletId}`);
		this.fetchReferences();
	}

	addNewReference = async data => {
		const { storyId } = this.props;
		await post(`/api/stories/${storyId}/references`, data);
		this.fetchReferences();
	}
	
	render() {
		const { references } = this.state;
		const { source } = this.props;

		return (
			<Paper className={styles.root}>
				<Typography
					align="center"
					variant="h5"
					className={styles.title}
				>
					Recorded Usage
				</Typography>
				<Typography
					variant="h6"
				>
					Source: {this.getSource(source)}
				</Typography>
				<ReferenceTimeline
					references={references}
					deleteReference={this.deleteReference}
					addNewReference={this.addNewReference}
				/>
			</Paper>
		);
	}
}
