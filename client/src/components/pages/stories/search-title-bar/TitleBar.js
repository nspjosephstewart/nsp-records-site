import React from 'react';

import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import LinearProgress from '@material-ui/core/LinearProgress';

import AppBarInput from '../../../elements/app-bar-input/AppBarInput';

import styles from './styles.less';

export default class TitleBar extends React.Component {
	state = {
		anchor: null,
	}
	
	openMenu = event => {
		this.setState({ anchor: event.target });
	}

	closeMenu = () => {
		this.setState({ anchor: null });
	}
	
	render() {
		const {
			query,
			onQueryChange,
			loadingQuery,
			showingPictures,
			showingStories,
			toggleShowPictures,
			toggleShowStories,
		} = this.props;

		const { anchor } = this.state;

		return (
			<Paper className={styles.root}>
				{loadingQuery ?
					<LinearProgress
						className={styles.linearProgress}
					/>
					:
					null
				}
				<Typography variant="h4">Stories</Typography>
				<div className={styles.spacer} />
				<AppBarInput
					value={query}
					icon={
						<Icon>search</Icon>
					}
					onChange={event => onQueryChange(event.target.value)}
					placeholder="Ctrl + F"
				/>
				<IconButton
					onClick={this.openMenu}
				>
					<Icon>
						more_vert
					</Icon>
				</IconButton>
				<Menu
					anchorEl={anchor}
					open={Boolean(anchor)}
					onClose={this.closeMenu}
				>
					<MenuItem
						onClick={toggleShowStories}
					>
						{showingStories ? 'Hide stories' : 'Show stories'}
					</MenuItem>
					<MenuItem
						onClick={toggleShowPictures}
					>
						{showingPictures ? 'Hide pictures' : 'Show pictures'}
					</MenuItem>
				</Menu>
			</Paper>
		)
	}
}
