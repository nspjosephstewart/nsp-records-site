import React from 'react';

import Checkbox from '../../../elements/form-components/Checkbox';

const TYPES = {
	'Rallies': 'rallies',
	'Witnessing Event': 'witnessing',
	'Story Capture': 'story',
}

export default ({ setFilter, filterName }) => (
	<Checkbox
		options={Object.keys(TYPES)}
		onChange={values =>
			setFilter(
				filterName,
				values.length > 0 ?
					values.map(value => TYPES[value]).join(encodeURIComponent('+'))
					:
					null,
				'in'
			)
		}
	/>
)
