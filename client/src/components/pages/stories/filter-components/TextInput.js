import React from 'react';

import TextField from '@material-ui/core/TextField';

import styles from './text-input.less';

export default ({ setFilter, filterName, ...rest }) => (
	<TextField
		{...rest}
		fullWidth
		className={styles.root}
		onChange={event =>
			setFilter(
				filterName,
				event.target.value,
				'like'
			)
		}
	/>
);
