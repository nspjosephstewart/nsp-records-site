import React from 'react';

import SchoolSelect from '../../../elements/form-components/SchoolSelect';

export default ({ setFilter, filterName }) => (
	<SchoolSelect
		multi
		useString
		includeAllSchools
		onChange={value =>
			setFilter(
				filterName,
				value ? value.replace(/\s*,\s*/ig, encodeURIComponent('+')) : null,
				'in'
			)
		}
	/>
);
