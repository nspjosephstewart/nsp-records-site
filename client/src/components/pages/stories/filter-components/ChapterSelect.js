import React from 'react';

import ChapterSelect from '../../../elements/form-components/ChapterSelect';

export default ({ setFilter, filterName }) => (
	<ChapterSelect
		multi
		onChange={value =>
			setFilter(
				filterName,
				value && value.length > 0 ? value.map(obj => obj.label).join(encodeURIComponent('+')) : null,
				'in'
			)
		}
	/>
)
