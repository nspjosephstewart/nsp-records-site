import React from 'react';

import FormCheckbox from '../../../elements/form-components/Checkbox';

export default ({ options, setFilter, filterName }) => (
	<FormCheckbox
		options={options}
		onChange={value => {
			if (value.length > 0) {
				setFilter(filterName, value.join(encodeURIComponent('+')), 'in');
			} else {
				setFilter(filterName, null);
			}
		}}
	/>
)
