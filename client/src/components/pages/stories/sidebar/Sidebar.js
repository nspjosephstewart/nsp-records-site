import React from 'react';

import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';

import Checkbox from '../filter-components/CheckBox';
import SchoolSelect from '../filter-components/SchoolSelect';
import ChapterSelect from '../filter-components/ChapterSelect';
import TextInput from '../filter-components/TextInput';
import TypeCheckbox from '../filter-components/TypeCheckbox';
import CollapsibleSection from '../collapsible-section/CollapsibleSection';

import styles from './styles.less';

export default class Sidebar extends React.Component {
	render() {
		const { setFilter } = this.props;

		return (
			<div className={styles.root}>
				<Paper className={styles.paper}>
					<Typography
						variant="h4"
						className={styles.title}
					>
						Filters
					</Typography>
					<CollapsibleSection
						title="Posted by"
					>
						<TextInput
							setFilter={setFilter}
							filterName="posted_by_user_name"
							placeholder="Posted by"
						/>
					</CollapsibleSection>
					<CollapsibleSection
						title="Chapter"
					>
						<ChapterSelect
							setFilter={setFilter}
							filterName="chapter"
						/>
					</CollapsibleSection>
					<CollapsibleSection
						title="School"
					>
						<SchoolSelect
							setFilter={setFilter}
							filterName="school"
						/>
					</CollapsibleSection>
					<CollapsibleSection
						title="State"
					>
						<Checkbox
							setFilter={setFilter}
							filterName="state"
							options={[
								'AZ',
								'CA',
								'IL',
								'NY',
								'WA',
							]}
						/>
					</CollapsibleSection>
					<CollapsibleSection
						title="Source"
					>
						<TypeCheckbox
							setFilter={setFilter}
							filterName="type"
						/>
					</CollapsibleSection>
					<CollapsibleSection
						title="Usage"
					>
						<TextInput
							setFilter={setFilter}
							filterName="event_names"
							placeholder="Used in event..."
						/>
						<TextInput
							setFilter={setFilter}
							filterName="social_media_names"
							placeholder="Used in social media..."
						/>
						<TextInput
							setFilter={setFilter}
							filterName="publication_names"
							placeholder="Used in publication..."
						/>
					</CollapsibleSection>
				</Paper>
			</div>
		)
	}
}
