import React from 'react';

import Icon from '@material-ui/core/Icon';
import Typography from '@material-ui/core/Typography';

import NewReferenceModal from '../new-reference-modal/NewReferenceModal';

import styles from './styles.less';

const classNames = (...classes) => classes.join(' ');

export default class ReferenceTimeline extends React.Component {
	state = {
		newReferenceModalOpen: false,
	}
	
	mapTimelineElement = reference => {
		const { deleteReference } = this.props;

		return (
			<div className={styles.timelineElement}>
				<div className={styles.dot} />
				<div className={styles.chip}>
					<Icon className={styles.icon}>
						{reference.type === 'e' ?
							'mic'
							:
							reference.type === 's' ?
								'computer'
								:
								'book'
						}
					</Icon>
					<Typography variant="body1">
						{reference.name}
					</Typography>
					<Icon
						className={styles.deleteIcon}
						onClick={() => deleteReference(reference.outlet_id)}
					>
						delete
					</Icon>
				</div>
			</div>
		)
	}

	onSubmit = data => {
		const { addNewReference } = this.props;
		this.setState({
			newReferenceModalOpen: false,
		}, () => addNewReference(data));
	}
	
	render() {
		const { references } = this.props;
		const { newReferenceModalOpen } = this.state;

		return (
			<div className={styles.root}>
				{references.map(this.mapTimelineElement)}
				<div className={styles.timelineElement}>
					<div className={styles.dot} />
					<div
						className={classNames(styles.chip, styles.new)}
						onClick={() => this.setState({ newReferenceModalOpen: true })}
					>
						New
					</div>
				</div>
				<div className={styles.line} />
				<NewReferenceModal
					open={newReferenceModalOpen}
					onClose={() => this.setState({ newReferenceModalOpen: false })}
					onSubmit={this.onSubmit}
				/>
			</div>
		)
	}
}
