import React from 'react';
import moment from 'moment';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';

import styles from './styles.less';

export default ({ storyId, schoolName, schoolId, timestamp, navigateBack }) => (
	<Paper
		className={styles.root}
	>
		<IconButton onClick={navigateBack} className={styles.backButton}>
			<Icon color="black">arrow_back</Icon>
		</IconButton>
		<Typography
			variant="h5"
			className={styles.title}
		>
			Story #{storyId} - <a href={`/school/${schoolId}`}>{schoolName}</a> - {moment(timestamp).format('MM/DD/YYYY')}
		</Typography>
	</Paper>
);
