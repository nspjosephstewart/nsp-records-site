import React from 'react';

import Sidebar from './sidebar/Sidebar';
import Content from './content/Content';
import SingleStoryPage from './single-story-page/SingleStoryPage';

import { get } from '../../../utils/fetch';
import debounce from '../../../utils/debounce';

import styles from './styles.less';

const LIMIT = 10;

export default class StoriesPage extends React.Component {
	state = {
		stories: [],
		pictures: [],
		activeFilters: {},
		query: '',
		loadingQuery: false,
		countStories: LIMIT,
		countPictures: LIMIT,
		view: 'search',
		storyId: null,
	}

	componentDidMount() {
		this.fetchStories();
		const urlPath = window.location.pathname;
		const storyId = parseInt(urlPath.split('/')[2]);

		if (!isNaN(storyId)) {
			this.setState({
				storyId,
				view: 'single',
			})
		}
	}

	fetchStories = debounce(
		async () => {
			const { activeFilters, query, countStories, countPictures } = this.state;
			const urlFilters = Object
				.keys(activeFilters)
				.map(
					columnName =>
						activeFilters[columnName] ?
							`${columnName}:${activeFilters[columnName].value}:${activeFilters[columnName].type}`
							:
							null
				)
				.filter(filter => filter)
				.join(',');
			
			const queryParams = [
				'full=true',
				query ? `query=${query}` : null,
				urlFilters ? `filters=${urlFilters}` : null,
			].filter(param => param).join('&');

			const storiesPromise = get(`/api/stories/stories?${queryParams}&limit=${countStories}`);
			const picturesPromise = get(`/api/stories/pictures?${queryParams}&limit=${countPictures}`);

			const [
				stories,
				pictures,
			] = await Promise.all([ storiesPromise, picturesPromise ]);

			this.setState({ stories, pictures, loadingQuery: false });
		},
		250
	)

	moreStories = () => this.setState(({ countStories }) => ({ countStories: countStories + LIMIT }), this.fetchStories);
	morePictures = () => this.setState(({ countPictures }) => ({ countPictures: countPictures + LIMIT }), this.fetchStories);

	setFilter = (filterName, value, type) => {
		this.setState(({ activeFilters }) => {
			const obj = { ...activeFilters };

			if (value) {
				obj[filterName] = { value, type };
			} else {
				obj[filterName] = undefined;
			}
			
			return {
				activeFilters: obj,
				loadingQuery: true,
				countStories: LIMIT,
				countPictures: LIMIT,
			};
		}, this.fetchStories);
	}

	onQueryChange = query => {
		this.setState({
			query,
			loadingQuery: true,
			countStories: LIMIT,
			countPictures: LIMIT,
		}, this.fetchStories);
	}

	navigateToStory = storyId => {
		this.setState({
			storyId,
			view: 'single',
		}, () => {
			window.history.pushState('', '', `/stories/${storyId}`);
		});
	}

	navigateBack = () => {
		this.setState({
			storyId: null,
			view: 'search',
			query: '',
			activeFilters: {},
			loadingQuery: true,
		}, () => {
			window.history.pushState('', '', '/stories');
			this.fetchStories();
		});
	}
	
	render() {
		const {
			stories,
			pictures,
			query,
			loadingQuery,
			activeFilters,
			view,
			storyId,
		} = this.state;

		if (view === 'single') {
			return (
				<SingleStoryPage
					storyId={storyId}
					navigateBack={this.navigateBack}
				/>
			)
		}

		return (
			<div className={styles.root}>
				<Sidebar
					activeFilters={activeFilters}
					setFilter={this.setFilter}
				/>
				<Content
					stories={stories}
					pictures={pictures}
					onQueryChange={this.onQueryChange}
					query={query}
					loadingQuery={loadingQuery}
					morePictures={this.morePictures}
					moreStories={this.moreStories}
					navigateToStory={this.navigateToStory}
				/>
			</div>
		);
	}
}
