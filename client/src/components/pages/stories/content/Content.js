import React from 'react';

import TitleBar from '../search-title-bar/TitleBar';
import PicturesSearchResults from '../pictures-search-results/PicturesSearchResults';
import StoriesSearchResults from '../stories-search-results/StoriesSearchResults';

import styles from './styles.less';

export default class Content extends React.Component {
	state = {
		showingStories: true,
		showingPictures: true,
	}

	toggleShowPictures = () => this.setState(
		({ showingPictures }) => ({ showingPictures: !showingPictures })
	)

	toggleShowStories = () => this.setState(
		({ showingStories }) => ({ showingStories: !showingStories })
	)
	
	render() {
		const {
			pictures,
			stories,
			query,
			onQueryChange,
			loadingQuery,
			moreStories,
			morePictures,
			navigateToStory,
		} = this.props;

		const {
			showingPictures,
			showingStories,
		} = this.state;

		return (
			<div className={styles.root}>
				<TitleBar
					query={query}
					onQueryChange={onQueryChange}
					loadingQuery={loadingQuery}
					showingPictures={showingPictures}
					showingStories={showingStories}
					toggleShowPictures={this.toggleShowPictures}
					toggleShowStories={this.toggleShowStories}
				/>
				<div className={styles.body}>
					{showingStories ?
						<StoriesSearchResults
							stories={stories}
							moreStories={moreStories}
							showingPictures={showingPictures}
							navigateToStory={navigateToStory}
						/>
						:
						null
					}
					{showingPictures ?
						<PicturesSearchResults
							pictures={pictures}
							morePictures={morePictures}
							showingStories={showingStories}
							navigateToStory={navigateToStory}
						/>
						:
						null
					}
				</div>
			</div>
		)
	}
}
