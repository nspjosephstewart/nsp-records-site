import React from 'react';

import CircularProgress from '@material-ui/core/CircularProgress';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import TitleBar from '../single-title-bar/SingleTitleBar';
import SingleStoryPictures from '../single-story-pictures/SingleStoryPictures';
import ReferenceSidebar from '../reference-sidebar/ReferenceSidebar';

import { get } from '../../../../utils/fetch';

import styles from './styles.less';

export default class SingleStoryPage extends React.Component {
	state = {
		story: null,
	}

	async componentDidMount() {
		const { storyId } = this.props;

		const story = await get(`/api/stories/${storyId}`);

		this.setState({
			story: story[0],
		});
	}
	
	render() {
		const { story } = this.state;
		const { storyId, navigateBack } = this.props;

		if (!story) {
			return (
				<div className={styles.spinner}>
					<CircularProgress />
				</div>
			)
		}

		return (
			<div className={styles.root}>
				<TitleBar
					storyId={storyId}
					schoolName={story.school_name}
					schoolId={story.school_id}
					timestamp={story.timestamp}
					navigateBack={navigateBack}
				/>
				<div className={styles.body}>
					<div className={styles.referenceSidebar}>
						<ReferenceSidebar
							storyId={storyId}
							source={story.type}
							sourceId={story.source_id}
						/>
					</div>
					<div className={styles.content}>
						<SingleStoryPictures
							pictures={story.picture_urls}
						/>
						<Paper className={styles.storyContainer}>
							<Typography variant="h5" className={styles.title}>
								Story - Posted by {story.posted_by_user_name}
							</Typography>
							<Typography variant="p">
								{story.text}
							</Typography>
						</Paper>
					</div>
				</div>
			</div>
		)
	}
}
