import React from 'react';

import moment  from 'moment';

import GridList from '@material-ui/core/GridList';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import GridListTile from '@material-ui/core/GridListTile';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import CardActionArea from '@material-ui/core/CardActionArea';

import styles from './styles.less';

export default class PicturesSearchResults extends React.Component {
	getTypeText = type => {
		switch(type) {
			case 'rallies':
				return 'Rally';
			case 'witnessing':
				return 'Witnessing Event';
			case 'Story':
				return 'Story Capture';
		}
	}
	
	mapTile = picture => {
		const { navigateToStory } = this.props;

		return (
			<GridListTile
				key={picture.picture_url}
			>
				<CardActionArea
					onClick={() => navigateToStory(picture.id)}
				>
					<img
						src={picture.picture_url && picture.picture_url.split(',')[0]}
						alt={picture.posted_by_user_name}
						key={picture.picture_url}
					/>
				</CardActionArea>
				<GridListTileBar
					title={picture.school}
					subtitle={
						`${picture.posted_by_user_name} - ${moment(picture.timestamp).format('MM/DD/YYYY')} - ${this.getTypeText(picture.type)}`
					}
				/>
			</GridListTile>
		)
	}
	
	render() {
		const { pictures, morePictures, showingStories } = this.props;

		return (
			<div className={styles.root}>
				<GridList cellHeight={200} cols={showingStories ? 2 : 4}>
					{pictures.map(this.mapTile)}
				</GridList>
				<div className={styles.buttonContainer}>
					<Button
						onClick={morePictures}
						color="primary"
					>
						More Pictures
					</Button>
				</div>
			</div>
		)
	}
}
