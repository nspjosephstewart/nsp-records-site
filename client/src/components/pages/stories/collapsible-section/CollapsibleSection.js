import React from 'react';

import Collapse from '@material-ui/core/Collapse';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';

import styles from './styles.less';

export default class CollapsibleSection extends React.Component {
	state = {
		expanded: false,
	}

	toggleExpanded = () => {
		this.setState(({ expanded }) => ({ expanded: !expanded }));
	}
	
	render() {
		const {
			title,
			children,
		} = this.props;

		const { expanded } = this.state;

		return (
			<div className={styles.root}>
				<div className={styles.titleContainer}>
					<Typography
						variant="h6"
						className={styles.title}
					>
						{title}
					</Typography>
					<IconButton
						onClick={this.toggleExpanded}
						size="small"
					>
						<Icon>
							{expanded ? 'expand_more' : 'expand_less'}
						</Icon>
					</IconButton>
				</div>
				<Divider className={styles.divider} />
				<Collapse in={expanded}>
					<div className={styles.content}>
						{children}
					</div>
				</Collapse>
			</div>
		)
	}
} 
