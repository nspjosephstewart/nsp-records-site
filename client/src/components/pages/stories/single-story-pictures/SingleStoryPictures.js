import React from 'react';

import Paper from '@material-ui/core/Paper';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import CardActionArea from '@material-ui/core/CardActionArea';
import Typography from '@material-ui/core/Typography';
import LightBox from 'react-images';

import styles from './styles.less';

export default class SingleStoryPictures extends React.Component {
	state = {
		lightBoxOpen: false,
		lightBoxIndex: 0,
	}
	
	mapGridListItem = (picture, lightBoxIndex) => (
		<GridListTile>
			<CardActionArea
				onClick={() => {
					this.setState({ lightBoxIndex, lightBoxOpen: true });
				}}
			>
				<img
					src={picture}
				/>
			</CardActionArea>
		</GridListTile>
	);
	
	render() {
		const { pictures } = this.props;
		const { lightBoxIndex, lightBoxOpen } = this.state;

		if (!pictures || pictures.length === 0) {
			return null;
		}
	
		return (
			<Paper className={styles.root}>
				<Typography variant="h5" className={styles.title}>Pictures</Typography>
				<GridList cols={3.5} className={styles.gridList}>
					{pictures.map(this.mapGridListItem)}
				</GridList>
				<LightBox
					images={pictures.map(src => ({ src, width: 2, height: 2 }))}
					onClose={() => this.setState({ lightBoxOpen: false })}
					onClickPrev={() =>
						this.setState({
							lightBoxIndex: (lightBoxIndex + pictures.length - 1) % pictures.length,
						})
					}
					onClickNext={() =>
						this.setState({
							lightBoxIndex: (lightBoxIndex + 1) % pictures.length,
						})
					}
					currentImage={lightBoxIndex}
					isOpen={lightBoxOpen}
				/>
			</Paper>
		)
	}
}

