import React from 'react';

import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

import Display from './Display';

import styles from './styles.less';

export default class StoriesSearchResults extends React.Component {
	mapStories = story => {
		const { navigateToStory } = this.props;
		return (
			<Grid item className={styles.gridItem} key={JSON.stringify(story)}>
				<Display
					title={story.school}
					author={story.posted_by_user_name}
					timestamp={story.timestamp}
					type={story.type}
					text={story.text}
					id={story.id}
					navigateToStory={navigateToStory}
				/>
			</Grid>
		)
	}
	
	render() {
		const { stories, moreStories } = this.props;
		return (
			<div className={styles.root}>
				<Grid container justify="center">
					{stories.map(this.mapStories)}
				</Grid>
				<div className={styles.buttonContainer}>
					<Button
						onClick={moreStories}
						color="primary"
					>
						More Stories
					</Button>
				</div>
			</div>
		)
	}
}
