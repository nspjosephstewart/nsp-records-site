import React from 'react';
import moment from 'moment';

import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import CardActionArea from '@material-ui/core/CardActionArea';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import styles from './styles.less';

export default class Display extends React.Component {
	getTypeText = type => {
		switch(type) {
			case 'rallies':
				return 'Rally';
			case 'witnessing':
				return 'Witnessing Event';
			case 'story':
				return 'Story Capture';
		}
	}
	
	render() {
		const {
			title,
			author,
			timestamp,
			type,
			text,
			id,
			navigateToStory,
		} = this.props;

		return (
			<Card className={styles.display}>
				<CardActionArea
					onClick={() => navigateToStory(id)}
				>
					<CardHeader
						title={title}
						classes={{
							title: styles.title,
							content: styles.cardTitleContent,
						}}
						subheader={`${author} - ${moment(timestamp).format('MM/DD/YYYY')} - ${this.getTypeText(type)}`}
					/>
					<div className={styles.spacer} />
					<CardContent className={styles.cardContent}>
						<Typography variant="p" className={styles.text}>
							{text}
						</Typography>
					</CardContent>
				</CardActionArea>
				<CardActions className={styles.cardActions}>
					<Button
						color="primary"
						className={styles.rightButton}
						onClick={() => navigateToStory(id)}
					>
						Read More
					</Button>
				</CardActions>
			</Card>
		)
	}
}
