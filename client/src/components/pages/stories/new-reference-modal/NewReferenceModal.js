import React from 'react';

import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';

import AutoComplete from '../../../elements/auto-complete/AutoComplete';
import CreateReference from './CreateReference';

import { get } from '../../../../utils/fetch';

import styles from './styles.less';

export default class NewReferenceModal extends React.Component {
	state = {
		selectedOutlet: {},
		creatingNew: false,
		referenceSelectFocused: false,
		type: '',
		url: '',
		printCount: '',
		medium: '',
		attendanceCount: '',
		speaker: '',
		date: '',
	}

	searchExistingOutlets = async searchValue => {
		const results = await get(`/api/stories/outlets/search${searchValue ? `?query=${searchValue}` : ''}`);
		return results.map(result => ({ label: result.name, value: result.outlet_id }));
	}

	handleChange = selectedOutlet => {
		this.setState({ selectedOutlet, creatingNew: false });
	}

	onNew = item => {
		this.setState({
			selectedOutlet: {
				label: item,
				value: -1
			},
			creatingNew: true
		});
	}

	onTypeChange = type => {
		this.setState({ type });
	}

	onSubmit = () => {
		const { onSubmit } = this.props;
		const {
			selectedOutlet,
			creatingNew,
			type,
			url,
			printCount,
			medium,
			attendanceCount,
			speaker,
			date,
		} = this.state;

		onSubmit({
			selectedOutlet,
			creatingNew,
			type,
			url,
			printCount,
			medium,
			attendanceCount,
			speaker,
			date,
		})
	}
	
	render() {
		const {
			open,
			onClose,
		} = this.props;
		const {
			selectedOutlet,
			creatingNew,
			referenceSelectFocused,
			type,
			url,
			printCount,
			medium,
			attendanceCount,
			speaker,
			date,
		} = this.state;

		return (
			<Dialog
				open={open}
				onClose={onClose}
				fullWidth
				maxWidth="xs"
			>
				<DialogTitle>Add Reference</DialogTitle>
				<DialogContent>
					<FormControl
						fullWidth
						className={styles.formControl}
					>
						<AutoComplete
							loadAsyncOptions={this.searchExistingOutlets}
							onChange={this.handleChange}
							value={selectedOutlet}
							textFieldProps={{
								InputLabelProps: {
									shrink: !!selectedOutlet.label || referenceSelectFocused
								},
								label: 'Reference'
							}}
							newOptionText="Create new:"
							onNew={this.onNew}
							onFocus={() => this.setState({ referenceSelectFocused: true })}
							onBlur={() => this.setState({ referenceSelectFocused: false })}
						/>
					</FormControl>
					{creatingNew ?
						<CreateReference
							onTypeChange={this.onTypeChange}
							type={type}
							url={url}
							onUrlChange={url => this.setState({ url })}
							printCount={printCount}
							medium={medium}
							onPrintCountChange={printCount => this.setState({ printCount })}
							onMediumChange={medium => this.setState({ medium })}
							attendanceCount={attendanceCount}
							speaker={speaker}
							onSpeakerChange={speaker => this.setState({ speaker })}
							onAttendanceCountChange={attendanceCount => this.setState({ attendanceCount })}
							date={date}
							onDateChange={date => this.setState({ date })}
						/>
						:
						null
					}
				</DialogContent>
				<DialogActions>
					<Button
						onClick={onClose}
					>
						Cancel
					</Button>
					<Button
						color="primary"
						onClick={this.onSubmit}
					>
						Submit
					</Button>
				</DialogActions>
			</Dialog>
		)
	}
}
