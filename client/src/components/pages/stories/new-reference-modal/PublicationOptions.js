import React from 'react';

import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';

import styles from './styles.less';

export default ({ onMediumChange, medium, printCount, onPrintCountChange }) => (
	<React.Fragment>
		<FormControl
			fullWidth
			className={styles.formControl}
		>
			<TextField
				value={printCount}
				type="number"
				onChange={event => onPrintCountChange(event.target.value)}
				label="Print count"
			/>
		</FormControl>
		<FormControl
			fullWidth
			className={styles.formControl}
		>
			<InputLabel htmlFor="medium-select">Medium</InputLabel>
			<Select
				inputProps={{ id: 'medium-select' }}
				value={medium}
				onChange={event => onMediumChange(event.target.value)}
			>
				<MenuItem value="email">Email</MenuItem>
				<MenuItem value="annual_report">Annual Report</MenuItem>
				<MenuItem value="training_materials">Training Materials</MenuItem>
				<MenuItem value="marketing_materials">Marketing Materials</MenuItem>
				<MenuItem value="other_printed">Other Printed Materials</MenuItem>
				<MenuItem value="other">Other</MenuItem>
			</Select>
		</FormControl>
	</React.Fragment>
);
