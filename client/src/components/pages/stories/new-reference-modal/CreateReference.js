import React from 'react';

import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';

import SocialMediaOptions from './SocialMediaOptions';
import PublicationOptions from './PublicationOptions';
import EventOptions from './EventOptions';

import styles from './styles.less';

export default class CreateReference extends React.Component {	
	renderOutletOptionControls = () => {
		const {
			type,
			url,
			onUrlChange,
			medium,
			printCount,
			onMediumChange,
			onPrintCountChange,
			attendanceCount,
			speaker,
			onAttendanceCountChange,
			onSpeakerChange,
		} = this.props;

		switch(type) {
			case 's':
				return (
					<SocialMediaOptions
						url={url}
						onUrlChange={onUrlChange}
					/>
				);
			case 'p':
				return (
					<PublicationOptions
						medium={medium}
						printCount={printCount}
						onMediumChange={onMediumChange}
						onPrintCountChange={onPrintCountChange}
					/>
				);
			case 'e':
				return (
					<EventOptions
						attendanceCount={attendanceCount}
						speaker={speaker}
						onAttendanceCountChange={onAttendanceCountChange}
						onSpeakerChange={onSpeakerChange}
					/>
				)
		}
	}
	
	render() {
		const {
			onTypeChange,
			type,
			date,
			onDateChange,
		} = this.props;

		return (
			<React.Fragment>
				<FormControl
					fullWidth
					className={styles.formControl}
				>
					<InputLabel htmlFor="type-select">Type</InputLabel>
					<Select
						inputProps={{ id: 'type-select' }}
						onChange={event => onTypeChange(event.target.value)}
						value={type}
					>
						<MenuItem
							value="s"
						>
							Social Media Platform
						</MenuItem>
						<MenuItem
							value="p"
						>
							Publication
						</MenuItem>
						<MenuItem
							value="e"
						>
							Event
						</MenuItem>
					</Select>
				</FormControl>
				{this.renderOutletOptionControls()}
				<FormControl
					fullWidth
					className={styles.formControl}
				>
					<TextField
						value={date}
						onChange={event => onDateChange(event.target.value)}
						type="date"
						label="Date"
						InputLabelProps={{ shrink: true }}
					/>
				</FormControl>
			</React.Fragment>
		)
	}
}
