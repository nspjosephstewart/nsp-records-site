import React from 'react';

import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';

import styles from './styles.less';

export default ({ onAttendanceCountChange, attendanceCount, speaker, onSpeakerChange }) => (
	<React.Fragment>
		<FormControl
			fullWidth
			className={styles.formControl}
		>
			<TextField
				label="Attendance count"
				value={attendanceCount}
				onChange={event => onAttendanceCountChange(event.target.value)}
				type="number"
			/>
		</FormControl>
		<FormControl
			fullWidth
			className={styles.formControl}
		>
			<TextField
				label="Speaker(s)"
				value={speaker}
				onChange={event => onSpeakerChange(event.target.value)}
			/>
		</FormControl>
	</React.Fragment>
);
