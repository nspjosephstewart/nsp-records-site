import React from 'react';

import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';

import styles from './styles.less';

export default ({ onUrlChange, url }) => (
	<React.Fragment>
		<FormControl
			fullWidth
			className={styles.formControl}
		>
			<TextField
				label="Post url"
				value={url}
				onChange={event => onUrlChange(event.target.value)}
			/>
		</FormControl>
	</React.Fragment>
);
