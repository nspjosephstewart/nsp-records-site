import React, {Component} from 'react';
import EditableField from '../editable-field/EditableField';
import styles from './styles.css';

/**
 * @description A perhaps misnamed component that displays some emphasized data along with a label
 * @todo Rename this
 */
class NumberDisplay extends Component {
	constructor(props) {
		super(props);

		this.state = {
			active: false
		}
	}

	toggleActive() {
		this.setState({active:!this.state.active})
	}

	render() {
		return (
			<div onClick={this.toggleActive.bind(this)} className={this.props.cssClass}>
				<div className={styles.numberDisplay}>
					{
						this.props.editing ?
							<EditableField
									obj={this.props.obj}
									label={this.props.objLabel}
									type={this.props.isNum ? "number" : "text"}
									changes={this.props.changes}
									setState={this.props.setState}
									editing={this.props.editing}
									isAdmin={this.props.isAdmin}
								/>
						:
							<div className={styles.numberDisplayNumber}>
									{
										this.props.number_display
									}
							</div>
					}

					<div className={styles.numberDisplayLabel}>
						{this.props.label}
					</div>
				</div>
			</div>
		)
	}
}

export default NumberDisplay;
