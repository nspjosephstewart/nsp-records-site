import React from 'react';
import MultiAutoComplete from '../auto-complete/MultiAutoComplete';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

import { get } from '../../../utils/fetch';

export default class ChapterSelect extends React.Component {
	state = {
		chapters: [],
		value: '',
	}
	
	loadChapters = async () => {
		const { multi } = this.props;
		if (multi) return;

		const chapters = await get(`/api/chapters/names`);
		this.setState({
			chapters: chapters.map(chapter => ({ label: chapter.name, value: chapter.id }))
		});
	}

	searchChapters = async searchValue => {
		const chapters = await get(`/api/chapters/names?${searchValue ? `query=${searchValue}&` : ''}count=10`);
		return chapters.map(chapter => ({ label: chapter.name, value: chapter.id }));
	}

	componentDidMount() {
		this.loadChapters();
	}

	mapSelectOptions = (chapter, index) => (
		<MenuItem
			value={chapter.value}
			key={index}
		>
			{chapter.label}
		</MenuItem>
	)

	handleChange = newValue => {
		const { onChange } = this.props;
		this.setState({
			value: newValue
		}, () => {
			const { value } = this.state;
			onChange(value);
		})
	}
	
	render() {
		const { multi, onChange, ...rest } = this.props;
		const { chapters, value } = this.state;

		if (multi) {
			return (
				<MultiAutoComplete
					loadAsyncOptions={this.searchChapters}
					onChange={this.handleChange}
					value={value}
					{...rest}
				/>
			)
		}
		
		return (
			<Select
				value={value}
				onChange={event => this.handleChange(event.target.value)}
			>
				{chapters.map(this.mapSelectOptions)}
			</Select>
		)
	}
}