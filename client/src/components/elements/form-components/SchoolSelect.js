import React from 'react';
import MultiAutoComplete from '../auto-complete/MultiAutoComplete';
import AutoComplete from '../auto-complete/AutoComplete';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

import { get } from '../../../utils/fetch';

export default class SchoolSelect extends React.Component {
	state = {
		schools: [],
		value: ''
	}
	
	loadSchools = async () => {
		const { multi, useSchoolId, includeAllSchools } = this.props;
		if (multi) return;

		const dataPoint = useSchoolId ? 'school_id' : 'registration_id';

		const schools = await get(`/api/schools/names?${!includeAllSchools ? 'enrolledSchoolsOnly=true' : ''}`);
		this.setState({
			schools: schools.map(school => ({ label: school.name, value: school[dataPoint] }))
		});
	}

	searchSchools = async searchValue => {
		const { useSchoolId, includeAllSchools } = this.props;
		const dataPoint = useSchoolId ? 'school_id' : 'registration_id';
		const schools = await get(`/api/schools/names?${searchValue ? `query=${searchValue}&` : ''}count=10${!includeAllSchools ? '&enrolledSchoolsOnly=true' : ''}`);
		return schools.map(school => ({ label: school.name, value: school[dataPoint] }));
	}

	componentDidMount() {
		this.loadSchools();
	}

	mapSelectOptions = school => (
		<MenuItem
			value={school.value}
		>
			{school.label}
		</MenuItem>
	)

	onNewSchool = schoolName => {
		const { useSchoolId, multi } = this.props;
		const { value } = this.state;
		const id = useSchoolId ? 238 : 903
		if (!multi) {
			this.handleChange({ label: schoolName, value: id });
		} else {
			const safeValue = value || [];
			this.handleChange([...safeValue, { label: schoolName, value: id }]);
		}
		if (this.input) {
			this.input.clearInput();
		}
	}

	handleChange = value => {
		const { onChange, useString, includeSchoolId } = this.props;
		this.setState({ value }, () => {
			const { value } = this.state;
			let change;
			if (useString && Array.isArray(value)) {
				change = value.map(obj => `${obj.label}${includeSchoolId ? ` (${obj.value})` : ''}`).join(', ');
			} else if (useString) {
				change = `${value.label}${includeSchoolId ? ` (${value.value})` : ''}`;
			} else if (Array.isArray(value)) {
				change = value.map(obj => obj.value);
			} else {
				change = value.value;
			}
			onChange(change);
		})
	}
	
	render() {
		const { allowNew, multi } = this.props;
		const { schools, value } = this.state;

		if (multi) {
			return (
				<MultiAutoComplete
					loadAsyncOptions={this.searchSchools}
					onChange={this.handleChange}
					value={value}
					newOptionText={allowNew ? 'School not listed:' : undefined}
					onNew={allowNew ? this.onNewSchool : undefined}
					innerRef={ref => (this.input = ref)}
				/>
			)
		}

		return (
			<AutoComplete
				loadAsyncOptions={this.searchSchools}
				onChange={this.handleChange}
				value={value}
				newOptionText={allowNew ? 'School not listed:' : undefined}
				onNew={allowNew ? this.onNewSchool : undefined}
				innerRef={ref => (this.input = ref)}
			/>
		)
		
		/*return (
			<Select
				value={value}
				onChange={event => this.handleChange(event.target.value)}
			>
				{schools.map(this.mapSelectOptions)}
			</Select>
		)*/
	}
}