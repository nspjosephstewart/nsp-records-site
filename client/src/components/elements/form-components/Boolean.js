import React from 'react';

import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';

let id = 0;
const generateName = () => `form_group_${id++}`;

export default class Bool extends React.Component {
	componentWillMount() {
		this.name = generateName();
	}
	
	render() {
		const { onChange } = this.props;
		return (
			<RadioGroup
				name={this.name}
				onChange={event => onChange(event.target.value === 'true')}
			>
				<FormControlLabel
					value="true"
					control={<Radio />}
					label="Yes"
				/>
				<FormControlLabel
					value="false"
					control={<Radio />}
					label="No"
				/>
			</RadioGroup>
		)
	}
}