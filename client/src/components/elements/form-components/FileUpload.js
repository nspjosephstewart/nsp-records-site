import React from 'react';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

let lastId = 0;

const newId = prefix => `${prefix}${lastId++}`;

export default class FileUpload extends React.Component {
	state = {
		value: []
	}
	
	componentWillMount() {
		this.inputId = newId('form_input_');
	}

	mapFileNames = (file, index) => (
		<Typography variant="p" key={index}>
			{file.name}
		</Typography>
	)

	handleChange = value => {
		const { onChange } = this.props;
		this.setState({ value }, () => onChange(this.state.value));
	}

	render() {
		const { value } = this.state;
		return (
			<div>
				<input
					type="file"
					accept="image/*"
					multiple
					style={{ display: 'none' }}
					id={this.inputId}
					onChange={event => this.handleChange(event.target.files)}
				/>
				<label htmlFor={this.inputId}>
					<Button
						component="span"
					>
						Upload Files
					</Button>
					{value && [...value].map(this.mapFileNames)}
				</label>
			</div>
		)
	}
}