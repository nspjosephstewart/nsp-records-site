import React from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import FormGroup from '@material-ui/core/FormGroup';

export default class FormCheckbox extends React.Component {
	
	state = {
		otherInputValue: '',
		value: {}
	}

	handleOtherInputChange = event => {
		const { onChange } = this.props;
		this.setState({ otherInputValue: event.target.value }, () => {
			const { otherInputValue, value } = this.state;
			if (value.other) {
				onChange([...Object.keys({ ...value, other: undefined }).filter(key => value[key]), otherInputValue]);
			}
		})
	}

	handleChange = name => event => {
		const { checked } = event.target;
		this.setState(({ value }) => ({
			value: {
				...value,
				[name]: checked
			}
		}), () => {
			const { onChange } = this.props;
			const { value, otherInputValue } = this.state;

			const obj = {...value, other: false};
			const keys = Object.keys(obj).filter(key => obj[key]);
			if (value.other) {
				keys.push(otherInputValue);
			}
			onChange(keys);
		})
	}

	mapOptions = (option, index) => {
		const { otherInputValue, value } = this.state;

		if (option === 'other') {
			return (
				<FormControlLabel
					value={option}
					checked={Boolean(value.other)}
					key={index}
					control={<Checkbox />}
					onChange={this.handleChange(option)}
					label={
						<TextField
							value={otherInputValue}
							onChange={this.handleOtherInputChange}
							placeholder="Other"
						/>
					}
				/>
			)
		}

		return (
			<FormControlLabel
				control={<Checkbox />}
				value={option}
				key={index}
				label={option}
				onChange={this.handleChange(option)}
			/>
		)
	}

	render() {
		const { options } = this.props;

		return (
			<FormGroup>
				{options && options.map(this.mapOptions)}
			</FormGroup>
		);
	}
}