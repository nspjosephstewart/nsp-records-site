import React from 'react';
import CreatableSelect from 'react-select/lib/Creatable';
import { withStyles } from '@material-ui/core/styles';
import styles from './styles';
import components from './components';

class CreatableAutoComplete extends React.Component {
	state = {
		value: null,
	};

	handleChange = value => {
		const { onChange } = this.props;

		if (onChange) {
			onChange(value);
		}

		this.setState({
			value: value,
		});
	};

	handleControlRef = ref => {
		this.controlRef = ref;
	}

	getMenuPositioning = () => {
		if (!this.controlRef) {
			return {
				width: 0,
				left: 0,
				bottom: 0,
			};
		}

		const { width, left, bottom } = this.controlRef.getBoundingClientRect();

		return { width, left, bottom };
	}

	render() {
		const { classes, theme, options, placeholder, onNew, formatCreateLabel, ...rest } = this.props;
		const { value } = this.state;

		const selectStyles = {
			input: base => ({
				...base,
				color: theme.palette.text.primary,
				'& input': {
					font: 'inherit',
				},
			}),
		};

		return (
			<CreatableSelect
				{...rest}
				classes={classes}
				styles={selectStyles}
				options={options}
				components={components}
				value={value}
				onChange={this.handleChange}
				placeholder={placeholder}
				maxMenuHeight={200}
				menuPlacement="auto"
				onCreateOption={onNew}
				formatCreateLabel={formatCreateLabel}
				controlRef={this.handleControlRef}
				getMenuPositioning={this.getMenuPositioning}
			/>
		);
	}
}

export default withStyles(styles, { withTheme: true })(CreatableAutoComplete);