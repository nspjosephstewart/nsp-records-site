import React from 'react';
import Select from 'react-select';
import AsyncSelect from 'react-select/lib/Async';
import { withStyles } from '@material-ui/core/styles';
import styles from './styles';
import components from './components';

class AutoComplete extends React.Component {
	state = {
		stateValue: null,
		inputValue: '',
	};

	handleChange = value => {
		const { onChange } = this.props;

		if (onChange) {
			onChange(value);
		}

		this.setState({
			stateValue: value,
			inputValue: '',
		});
	};

	clearInput = () => {
		this.setState({ inputValue: '' });
	}

	handleControlRef = ref => {
		this.controlRef = ref;
	}

	getMenuPositioning = () => {
		if (!this.controlRef) {
			return {
				width: 0,
				left: 0,
				bottom: 0,
			};
		}

		const { width, left, bottom } = this.controlRef.getBoundingClientRect();

		return { width, left, bottom };
	}

	onNew = item => {
		const { onNew } = this.props;

		if (onNew) {
			onNew(item);
		}

		if (this.selectRef) {
			this.selectRef.select.select.blur();
		}
	}

	handleRef = ref => this.selectRef = ref;

	render() {
		const { classes, value, theme, options, placeholder, onNew, loadAsyncOptions, ...rest } = this.props;
		const { stateValue, inputValue } = this.state;

		const selectStyles = {
			input: base => ({
				...base,
				color: theme.palette.text.primary,
				'& input': {
					font: 'inherit',
				},
			}),
		};

		let SelectComponent = Select;
		if (loadAsyncOptions) {
			SelectComponent = AsyncSelect;
		}

		return (
			<SelectComponent
				{...rest}
				classes={classes}
				styles={selectStyles}
				options={options}
				components={components}
				value={value || stateValue}
				onChange={this.handleChange}
				placeholder={placeholder}
				maxMenuHeight={200}
				menuPlacement="auto"
				onNew={onNew ? this.onNew : undefined}
				ref={this.handleRef}
				onInputChange={inputValue => this.setState({ inputValue })}
				inputValue={inputValue}
				loadOptions={loadAsyncOptions}
				cacheOptions={Boolean(loadAsyncOptions)}
				defaultOptions={Boolean(loadAsyncOptions)}
				controlRef={this.handleControlRef}
				getMenuPositioning={this.getMenuPositioning}
			/>
		);
	}
}

export default withStyles(styles, { withTheme: true })(AutoComplete);