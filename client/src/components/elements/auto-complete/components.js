import React from 'react';
import ReactDOM from 'react-dom';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Chip from '@material-ui/core/Chip';
import MenuItem from '@material-ui/core/MenuItem';
import Icon from '@material-ui/core/Icon';
import ListItem from '@material-ui/core/ListItem';
import classNames from 'classnames';

function NoOptionsMessage(props) {
	return (
		<ListItem><i>{props.selectProps.noOptions || 'No options'}</i></ListItem>
	);
}

function inputComponent({ inputRef, ...props }) {
	return <div ref={inputRef} {...props} />;
}

function Control(props) {
	return (
		<div ref={props.selectProps.controlRef}>
			<TextField
				fullWidth
				InputProps={{
					inputComponent,
					inputProps: {
						className: props.selectProps.classes.input,
						inputRef: props.innerRef,
						children: props.children,
						...props.innerProps,
					},
				}}
				{...props.selectProps.textFieldProps}
			/>
		</div>
	);
}

function Option(props) {
	return (
		<MenuItem
			buttonRef={props.innerRef}
			selected={props.isFocused}
			component="div"
			style={{
				fontWeight: props.isSelected ? 500 : 400,
			}}
			{...props.innerProps}
		>
			{props.children}
		</MenuItem>
	);
}

function Placeholder(props) {
	return (
		<Typography
			color="textSecondary"
			className={props.selectProps.classes.placeholder}
			{...props.innerProps}
		>
			{props.children}
		</Typography>
	);
}

function SingleValue(props) {
	return (
		<Typography className={props.selectProps.classes.singleValue} {...props.innerProps}>
			{props.children}
		</Typography>
	);
}

function ValueContainer(props) {
	return <div className={props.selectProps.classes.valueContainer}>{props.children}</div>;
}

function MultiValue(props) {
	return (
		<Chip
			tabIndex={-1}
			label={props.children}
			className={classNames(props.selectProps.classes.chip, {
				[props.selectProps.classes.chipFocused]: props.isFocused,
			})}
			onDelete={props.removeProps.onClick}
			deleteIcon={<Icon {...props.removeProps}>cancel</Icon>}
		/>
	);
}

class Menu extends React.Component {
	constructor(props) {
		super(props);
		this.root = document.createElement('div');
		this.root.classList.add(props.selectProps.classes.paperRoot);
	}

	componentDidMount() {
		document.body.appendChild(this.root);
	}

	componentWillUnmount() {
		document.body.removeChild(this.root);
	}
	
	render() {
		const props = this.props;
		const { width, bottom, left } = props.selectProps.getMenuPositioning();

		return ReactDOM.createPortal(
			<Paper
				square
				className={props.selectProps.classes.paper}
				{...props.innerProps}
				style={{
					position: 'absolute',
					top: bottom,
					width,
					left,
				}}
			>
				{props.children}
			</Paper>,
			this.root
		);
	}
}

function MenuList(props) {
	const { children, selectProps, maxHeight } = props;
	const styles = { maxHeight, overflowY: 'auto' };
	return (
		<div style={styles}>
			{children}
			{
				selectProps.onNew && selectProps.inputValue && !selectProps.isLoading ?
					(
						<MenuItem
							onClick={() => selectProps.onNew(selectProps.inputValue)}
						>
							{selectProps.newOptionText || 'Enroll new school:'}&nbsp;<strong>{selectProps.inputValue}</strong>
						</MenuItem>
					)
				:
					null
			}
		</div>
	)
}

export default {
	Control,
	Menu,
	MultiValue,
	NoOptionsMessage,
	Option,
	Placeholder,
	SingleValue,
	ValueContainer,
	MenuList,
};