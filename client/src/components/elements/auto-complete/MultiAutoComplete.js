import AutoComplete from './AutoComplete';
import React from 'react';

export default props => (
	<AutoComplete
		isMulti
		{...props}
	/>
)