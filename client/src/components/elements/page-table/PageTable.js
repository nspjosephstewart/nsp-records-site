import React from 'react';

import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import TableBody from '@material-ui/core/TableBody';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import TablePagination from '@material-ui/core/TablePagination';
import CircularProgress from '@material-ui/core/CircularProgress';
import LinearProgress from '@material-ui/core/LinearProgress';

import styles from './styles.css';

const breakpoints = {
	xs: 0,
	sm: 600,
	md: 960,
	lg: 1280,
	xl: 1920,
}

const breakpointsArray = [
	0,
	600,
	960,
	1280,
	1920,
]

/**
 * @description Paginated table that takes a regular HTML table as it's child
 */
export default class PageTable extends React.Component {

	state = {
		orderBy: null,
		direction: 'asc',
		page: 0,
		rowsPerPage: 10,
		lastBreakpoint: 1920,
	}

	createSortHandler = property => event => {
		this.handleRequestSort(event, property);
	};

	handleRequestSort = (event, property) => {
		const orderBy = property;
		let direction = 'desc';

		if (this.state.orderBy === property && this.state.direction === 'desc') {
			direction = 'asc';
		}

		this.setState({ direction, orderBy });
	};


	desc = (a, b, orderBy) => {
		if (b[orderBy] < a[orderBy]) {
			return -1;
		}
		if (b[orderBy] > a[orderBy]) {
			return 1;
		}
		return 0;
	}

	stableSort = (array, cmp) => {

		const stabilizedThis = array.map((el, index) => [el, index]);

		stabilizedThis.sort((a, b) => {
			const order = cmp(a[0], b[0]);
			if (order !== 0) return order;
			return a[1] - b[1];
		});
		return stabilizedThis.map(el => el[0]);
	}

	getSorting = (order, orderBy) => {
		return order === 'desc' ? (a, b) => this.desc(a, b, orderBy) : (a, b) => -this.desc(a, b, orderBy);
	}

	handleChangePage = (event, page) => {
		this.setState({ page });
	};

	handleChangeRowsPerPage = event => {
		this.setState({ rowsPerPage: event.target.value });
	};

	getTableHeadCell = key => {
		const { head } = this.props;
		const { lastBreakpoint } = this.state;
		const row = head[key];

		if (breakpoints[row.hideBreakpoint] && breakpoints[row.hideBreakpoint] >= lastBreakpoint)
			return null;

		if (row.getHeadComponent) {
			return (
				<TableCell
					padding={row.checkbox ? 'checkbox' : undefined}
				>
					{row.getHeadComponent()}
				</TableCell>
			)
		}
		else if (!row.name) {
			return <TableCell/>;
		}

		const { orderBy, direction } = this.state;

		return (
			<TableCell>
				<TableSortLabel
					active={orderBy === key}
					direction={direction}
					onClick={this.createSortHandler(key)}
				>
					{row.name}
				</TableSortLabel>
			</TableCell>
		)
	}

	getTableBodyCell = (row, key) => {
		const { head } = this.props;
		const { lastBreakpoint } = this.state;
		const headObj = head[key];

		if (breakpoints[headObj.hideBreakpoint] && breakpoints[headObj.hideBreakpoint] >= lastBreakpoint)
			return null;
		
		return (
			<TableCell
				padding={headObj.dense ? 'dense' : undefined}
			>
				{headObj.getCell ? 
					headObj.getCell(row[key], row)
				:
					row[key]
				}
			</TableCell>
		)
	}

	getTableBodyRow = row => {
		const { head, rowIsSelected, onRowClicked } = this.props;
		return (
			<TableRow
				hover
				onClick={onRowClicked && (() => onRowClicked(row))}
				style={{ cursor: onRowClicked ? 'pointer' : undefined }}
				selected={rowIsSelected && rowIsSelected(row)}
			>
				{Object.keys(head).map(key => this.getTableBodyCell(row, key))}
			</TableRow>
		)
	}

	handleResize = event => this.setLastBreakpoint(event.currentTarget);

	setLastBreakpoint = win => {
		const { lastBreakpoint } = this.state;
		const newWidth = win.innerWidth;
		for (let i = 0; i < breakpointsArray.length; i++) {
			const breakpoint = breakpointsArray[i];
			if (newWidth < breakpoint) {
				if (lastBreakpoint === breakpoint)
					return;
				this.setState({
					lastBreakpoint: breakpoint,
				});
				return;
			}
		}
	}

	componentDidMount() {
		window.addEventListener('resize', this.handleResize);
		this.setLastBreakpoint(window);
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this.handleResize);
	}

	render() {
		const { data, head, flat, minWidth, Toolbar, noMargin, loading } = this.props;
		const { direction, orderBy, rowsPerPage, page } = this.state;

		const inlineStyle = { minWidth: minWidth || breakpoints.sm }

		return (
			<div className={!noMargin ? styles.container : undefined}>
				<Paper elevation={flat ? 1 : 2}>
					{loading && data.length > 0 ?
						<LinearProgress />
					:
						null
					}
					{Toolbar && Toolbar}
					{
						loading && data.length === 0 ?
							<div className={styles.loadingSpinner}>
								<CircularProgress />
							</div>
						:
							<React.Fragment>
								<div className={styles.tableWrapper}>
									<Table style={inlineStyle}>
										<TableHead>
											<TableRow>
												{Object.keys(head).map(this.getTableHeadCell)}
											</TableRow>
										</TableHead>
										<TableBody>
											{
												this.stableSort(data, this.getSorting(direction, orderBy))
												.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
												.map(this.getTableBodyRow)
											}
										</TableBody>
									</Table>
								</div>
								<TablePagination
									rowsPerPageOptions={[10, 25, 50, 100]}
									component="div"
									count={data.length}
									rowsPerPage={rowsPerPage}
									page={page}
									onChangePage={this.handleChangePage}
									onChangeRowsPerPage={this.handleChangeRowsPerPage}
								/>
							</React.Fragment>
					}
				</Paper>
			</div>
		)
	}

}
