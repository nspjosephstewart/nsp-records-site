import React from 'react';
import { get } from '../../../../utils/fetch';

export default class IfUserIsAdmin extends React.Component {
	state = {
		userIsAdmin: false,
		isLoaded: false,
		isError: false,
	}

	componentDidMount() {
		get('/api/user').then(
			user => {
				this.setState({
					isLoaded: true,
					userIsAdmin: user.isAdmin,
				});
			}
		)
	}

	render() {
		const { isLoaded, userIsAdmin, isError } = this.state;
		const { ifNotAdminComponent, children, errorComponent } = this.props;

		if (!isLoaded)
			return null;
		
		if (isError)
			return errorComponent || null;

		return (
			<React.Fragment>
				{
					userIsAdmin ?
						children
					: ifNotAdminComponent ? 
						ifNotAdminComponent
					: null
				}
			</React.Fragment>
		)
	}
}