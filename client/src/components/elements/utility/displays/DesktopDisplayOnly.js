import React from 'react';

const breakpoints = {
	xs: 0,
	sm: 600,
	md: 960,
	lg: 1280,
	xl: 1920,
}

export default class MobileDisplayOnly extends React.Component {
	state = {
		onDesktop: false,
	}
	
	componentDidMount() {
		window.addEventListener('resize', this.resizeCallback);
		const { breakpoint } = this.props;
		const breakpointValue = breakpoints[breakpoint] || breakpoints.sm;

		this.setState({
			onDesktop: (window.innerWidth >= breakpointValue)
		});
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this.resizeCallback);
	}

	resizeCallback = event => {
		const { breakpoint } = this.props;
		const { onDesktop } = this.state;
		const breakpointValue = breakpoints[breakpoint] || breakpoints.sm;
		
		if ((event.currentTarget.innerWidth >= breakpointValue) && !onDesktop) {
			this.setState({
				onDesktop: true,
			});
		} else if ((event.currentTarget.innerWidth < breakpointValue) && onDesktop) {
			this.setState({
				onDesktop: false,
			})
		}
	}
	
	render() {
		const { inline, mobileComponent, children, wrapperProps, ...rest } = this.props;
		const { onDesktop } = this.state;

		const styles = {
			display: inline ? 'inline' : 'block'
		};

		if (!onDesktop && mobileComponent) {
			<div style={styles} {...rest}>
				{mobileComponent}
			</div>
		} else if (!onDesktop) {
			return null;
		}

		return (
			<div style={styles} {...rest}>
				{children}
			</div>
		)
	}
}