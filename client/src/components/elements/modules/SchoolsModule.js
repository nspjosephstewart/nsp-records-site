import React, {Component} from 'react';
import {Link} from 'react-router-dom';

import _ from 'lodash';
import 'whatwg-fetch';

import PageTable from '../page-table/PageTable'
import Card from '../card/Card';
import NumberDisplay from '../number-display/NumberDisplay';
import CollapseButton from '../collapse-button/CollapseButton';

import analytics from '../../../utils/analytics';

class SchoolsModule extends Component {
	constructor(props) {
		super(props);

		this.state = {
			chapter_ids: props.chapter_ids,
			collapsed: props.collapsed,
			current_range_school_count: props.schools.length,
			schools: props.schools,
			sort_by: props.sort_by,
			unique_school_count: 0,
			max_date: props.max_date,
			min_date: props.min_date,
			reverse_sort_by: false
		};

		this.handleCollapseState = this.handleCollapseState.bind(this);
		this.handleSortBy = this.handleSortBy.bind(this);
	}

	handleCollapseState() {
		this.setState(
			{
				collapsed: !this.state.collapsed
			}
		)
	}

	removeAllClassesFromElements(nodes) {
		for (let index = 0; index < nodes.length; index++) {
			let node = nodes[index];

			node.classList = '';
		}
	}

	handleSortBy(sort_by, event) {
		this.removeAllClassesFromElements(event.target.parentNode.children);

		if (sort_by == this.state.sort_by) {
			let newSortByOrder = !this.state.reverse_sort_by;

			this.setState(
				{
					reverse_sort_by: newSortByOrder,
					schools: _.orderBy(this.state.schools, [`${this.state.sort_by}`], [`${newSortByOrder ? 'desc' : 'asc'}`])
				}
			)

			event.target.classList.add(`mdl-data-table__header--sorted-${newSortByOrder ? 'descending' : 'ascending'}`);
		}
		else {
			this.setState(
				{
					reverse_sort_by: false,
					sort_by: sort_by,
					schools: _.orderBy(this.state.schools, [`${sort_by}`], [`asc`])
				}
			);

			event.target.classList.add(`mdl-data-table__header--sorted-ascending`);

		}
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.chapter_ids != nextProps.chapter_ids || nextProps.distanceOnly !== this.props.distanceOnly) {
			this.setState(
				{
					chapter_ids: nextProps.chapter_ids
				}
			)
			this.getUniqueSchoolCountForAllTime(nextProps.distanceOnly);
		}

		if (this.props.schools != nextProps.schools) {
			this.setState(
				{
					current_range_school_count: nextProps.schools.length,
					schools: _.orderBy(nextProps.schools, [`${this.state.sort_by}`], [`${this.state.reverse_sort_by ? 'desc' : 'asc'}`])
				}
			)
		}

		if (nextProps.collapsed != this.props.collapsed) {
			this.setState(
				{
					collapsed: nextProps.collapsed
				}
			)
		}

		if (nextProps.max_date != this.props.max_date) {
			this.setState(
				{
					max_date: nextProps.max_date
				}
			)
		}

		if (nextProps.min_date != this.props.min_date) {
			this.setState(
				{
					min_date: nextProps.min_date
				}
			)
		}
	}

	componentDidMount() {
		this.getUniqueSchoolCountForAllTime(this.props.distanceOnly);
	}

	getUniqueSchoolCountForAllTime = (distanceOnly) => {
		const { chapter_ids } = this.state;
		const { allChapters } = this.props;

		const distanceOnlyQuery = distanceOnly ? '{distance=1}' : ''

		analytics.count(`schools ${distanceOnlyQuery} ${!allChapters ? 'FOR EACH chapter' : ''}`)
		.then(results => {
			if (results.result) {
				this.setState({
					unique_school_count: results.result
				})
			}
			else {
				this.setState({
					unique_school_count: chapter_ids.reduce(
						(accumulator, key) => (accumulator+(results[key] || 0)),
						0
					) || 0
				});
			}
		});
	}

	render() {
		return (
			<Card cssClass={this.props.cssClass}>
				<div className="mdl-grid mdl-grid-centered">
					<div className="mdl-cell mdl-cell--2-col">
						<h4>Schools</h4>
					</div>

					<div className="mdl-cell mdl-cell--10-col mdl-grid">
						<div className="mdl-cell">
							<NumberDisplay
								label="All time unique schools"
								number_display={this.state.unique_school_count.toLocaleString()}
							/>
						</div>

						<div className="mdl-cell">
							<NumberDisplay
								label="Unique Schools in range"
								number_display={this.state.current_range_school_count.toLocaleString()}
							/>
						</div>
					</div>
				</div>
				{
					this.state.current_range_school_count > 0 ?
						<div className="mdl-grid">
								<CollapseButton
									collapsed={this.state.collapsed}
									collapsed_label="More"
									onClick={this.handleCollapseState}
									raised={false}
									uncollapsed_label="Less"
								/>

								{!this.state.collapsed ?
								<PageTable 
									cssClass='mdl-data-table mdl-js-data-table mdl-cell mdl-cell--12-col'
									head={{
										'name': {
											name: 'Name',
											onClick: (event) => this.handleSortBy('name', event),
											getCell: (value, row) => 
												<Link to={`/school/${row.school_id}`}>{value}</Link>
										},
										'county': {
											name: 'County',
											onClick: (event) => this.handleSortBy('county', event)
										},
										'state': {
											name: 'State',
											onClick: (event) => this.handleSortBy('state', event)
										},
										'chapter_name': {
											name: 'Chapter',
											onClick: (event) => this.handleSortBy('chapter_name', event),
											getCell: (value, row) =>
												<Link to={`/chapter/custom?ids=${row.chapter_id}&min_date=${this.state.min_date}&max_date=${this.state.max_date}&query_title=${value}`}>
													{value}
												</Link>
										},
									}}
									data={this.state.schools}
								/>
								:''}
						</div>
						: ''
				}
			</Card>
		)
	}
}

export default SchoolsModule;
