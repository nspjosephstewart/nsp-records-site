import React, {Component} from 'react';
import {Link} from 'react-router-dom';

import moment from 'moment';
import _ from 'lodash';
import 'whatwg-fetch';

import PageTable from '../page-table/PageTable';
import Card from '../card/Card';
import NumberDisplay from '../number-display/NumberDisplay';
import CollapseButton from '../collapse-button/CollapseButton';

class WitnessingModule extends Component {
	constructor(props) {
		super(props);

		this.state = {
			collapsed: props.collapsed,
			max_date: props.max_date,
			min_date: props.min_date,
			reverse_sort_by: false,
			sort_by: props.sort_by,
			total_events: 0,
			witnessing_days: props.witnessing_days,
		};

		this.handleCollapseState = this.handleCollapseState.bind(this);
		this.handleSortBy = this.handleSortBy.bind(this);
	}

	handleCollapseState() {
		this.setState(
			{
				collapsed: !this.state.collapsed
			}
		)
	}

	removeAllClassesFromElements(nodes) {
		for (let index = 0; index < nodes.length; index++) {
			let node = nodes[index];

			node.classList = '';
		}
	}

	handleSortBy(sort_by, event) {
		this.removeAllClassesFromElements(event.target.parentNode.children);

		if (sort_by == this.state.sort_by) {
			let newSortByOrder = !this.state.reverse_sort_by;

			this.setState(
				{
					reverse_sort_by: newSortByOrder,
					witnessing_days: _.orderBy(this.state.witnessing_days, [`${this.state.sort_by}`], [`${newSortByOrder ? 'desc' : 'asc'}`])
				}
			)

			event.target.classList.add(`mdl-data-table__header--sorted-${newSortByOrder ? 'descending' : 'ascending'}`);
		}
		else {
			this.setState(
				{
					reverse_sort_by: false,
					sort_by: sort_by,
					witnessing_days: _.orderBy(this.state.witnessing_days, [`${sort_by}`], [`asc`])
				}
			);

			event.target.classList.add(`mdl-data-table__header--sorted-ascending`);
		}
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.witnessing_days != nextProps.witnessing_days) {
			this.setState(
				{
					witnessing_days: _.orderBy(nextProps.witnessing_days, [`${this.state.sort_by}`], [`${this.state.reverse_sort_by ? 'desc' : 'asc'}`])
				}
			)
		}

		if (nextProps.collapsed != this.props.collapsed) {
			this.setState(
				{
					collapsed: nextProps.collapsed
				}
			)
		}

		if (nextProps.max_date != this.props.max_date) {
			this.setState(
				{
					max_date: nextProps.max_date
				}
			)
		}

		if (nextProps.min_date != this.props.min_date) {
			this.setState(
				{
					min_date: nextProps.min_date
				}
			)
		}
	}

	render() {
		return (
			<Card cssClass={this.props.cssClass}>
				<div className="mdl-grid mdl-grid-centered">
					<div className="mdl-cell mdl-cell--2-col">
						<h4>Witnessing Days</h4>
					</div>

					<div className="mdl-cell mdl-cell--10-col mdl-grid">
						<div className="mdl-cell--3-col mdl-cell--12-col-phone mdl-cell--12-col-tablet">
							<NumberDisplay
								label="All Time Witnessing Days"
								number_display={this.props.all_time_count}
							/>
						</div>

						<div className="mdl-cell--3-col mdl-cell--12-col-phone mdl-cell--12-col-tablet">
							<NumberDisplay
								label="Total witnessing days in this period"
								number_display={this.state.witnessing_days.length.toLocaleString()}
							/>
						</div>

						<div className="mdl-cell--3-col mdl-cell--12-col-phone mdl-cell--12-col-tablet">
							<NumberDisplay
								label="Total gospel presentations in this period"
								number_display={_.sumBy(this.props.witnessing_days, 'gospel_presentations')}
							/>
						</div>

						<div className="mdl-cell--3-col mdl-cell--12-col-phone mdl-cell--12-col-tablet">
							<NumberDisplay
								label="Total decisions indicated in this period"
								number_display={_.sumBy(this.props.witnessing_days, 'indicated_decisions')}
							/>
						</div>
					</div>
				</div>

				{
					this.state.witnessing_days.length > 0 ?
						<div className="mdl-grid">
							<CollapseButton
								collapsed={this.state.collapsed}
								collapsed_label="More"
								onClick={this.handleCollapseState}
								raised={false}
								uncollapsed_label="Less"
							/>

							<div className = {this.state.collapsed ? "hidden table-responsive mdl-cell mdl-cell--12-col mdl-grid" : "table-responsive mdl-cell mdl-cell--12-col mdl-grid"}>
								<PageTable
									cssClass="mdl-data-table mdl-js-data-table mdl-cell mdl-cell--12-col"
									data={this.state.witnessing_days}
									head={
										{
											'name': {
												name: 'School',
												onClick: (event) => this.handleSortBy('name', event),
												getCell: (value, row) => 
													<Link to={`/school/${row.school_id}`}>
														{value}
													</Link>
											},
											'location': {
												name: 'Location',
												onClick: (event) => this.handleSortBy('location', event)
											},
											'date': {
												name: 'Date',
												onClick: (event) => this.handleSortBy('date', event),
												getCell: (value) => moment(value).format('MMMM Do YYYY')
											},
											'gospel_presentations': {
												name: 'Gospel Presentations',
												onClick: (event) => this.handleSortBy('gospel_presentations', event)
											},
											'indicated_decisions': {
												name: 'Indicated Decisions',
												onClick: (event) => this.handleSortBy('indicated_decisions', event)
											},
											'chapter_name': {
												name: 'Chapter',
												onClick: (event) => this.handleSortBy('chapter_name', event),
												getCell: (value, row) =>
													<Link to={`/chapter/custom?ids=${row.chapter_id}&min_date=${this.state.min_date}&max_date=${this.state.max_date}&query_title=${value}`}>
														{value}
													</Link>
											},
											'id': {
												getCell: (value) =>
													<Link to={`/witnessingday/${value}`}>
														<i className="material-icons">add</i>
													</Link>
											}
										}
									}
								/>
							</div>
						</div>
						:

						''
					}

			</Card>
		)
	}
}

export default WitnessingModule;
