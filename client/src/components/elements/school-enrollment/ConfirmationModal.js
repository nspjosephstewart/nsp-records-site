import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';

export default props => {
	const {
		onClose,
		open,
		onSubmit,
		selectedSchools,
		text
	} = props;

	return (
		<Dialog
			open={open}
			onClose={onClose}
			maxWidth="xs"
		>
			<DialogTitle>
				Enroll Schools?
			</DialogTitle>
			<DialogContent>
				<DialogContentText>
					{
						text ?
							<span>{text}</span>
						: 
							<span>Are you sure you wish to enroll the following schools: <strong>{selectedSchools.map(obj => obj.label).join(', ')}</strong>?</span>
					}
				</DialogContentText>
			</DialogContent>
			<DialogActions>
				<Button onClick={onClose}>
					Cancel
				</Button>
				<Button onClick={onSubmit} color="primary" autoFocus>
					Enroll
				</Button>
			</DialogActions>
		</Dialog>
	)
}