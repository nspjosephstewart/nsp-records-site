import React from 'react';

import styles from './styles.less';

export default class Gallery extends React.Component {
	mapImage = (image, index) => {
		const { onClick } = this.props;
		return (
			<div className={styles.imageContainer}>
				<img
					className={styles.image}
					src={image.src}
					onClick={event => {
						onClick(event, { index });
					}}
				/>
			</div>
		)
	}
	
	render() {
		const {
			photos
		} = this.props;

		return (
			<div className={styles.root}>
				{photos.map(this.mapImage)}
			</div>
		)
	}
}