import React from 'react';
import InputBase from '@material-ui/core/InputBase';
import { withStyles } from '@material-ui/core/styles';
import { darken } from '@material-ui/core/styles/colorManipulator';

const styles = theme => ({
	search: {
		position: 'relative',
		borderRadius: theme.shape.borderRadius,
		backgroundColor: darken(theme.palette.common.white, 0.15),
		'&:hover': {
			backgroundColor: darken(theme.palette.common.white, 0.25),
		},
		marginRight: theme.spacing.unit * 2,
		marginLeft: 0,
		width: '100%',
		[theme.breakpoints.up('sm')]: {
			marginLeft: theme.spacing.unit * 3,
			width: 'auto',
		},
	},
	searchIcon: {
		width: theme.spacing.unit * 5,
		height: '100%',
		position: 'absolute',
		pointerEvents: 'none',
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
	},
	inputRoot: {
		color: 'inherit',
		width: '100%',
	},
	inputInput: {
		paddingTop: theme.spacing.unit,
		paddingRight: theme.spacing.unit,
		paddingBottom: theme.spacing.unit,
		paddingLeft: theme.spacing.unit * 5,
		transition: theme.transitions.create('width'),
		width: '100%',
		[theme.breakpoints.up('sm')]: {
			width: 150,
			'&:focus': {
				width: 200,
			},
		},
	},
});

const AppBarInput = props => {
	const { classes, icon, placeholder, ...rest } = props;

	return (
		<div className={classes.search}>
			<div className={classes.searchIcon}>
				{icon}
			</div>
			<InputBase
				placeholder={placeholder}
				{...rest}
				classes={{
					root: classes.inputRoot,
					input: classes.inputInput,
				}}
			/>
		</div>
	)
}

export default withStyles(styles)(AppBarInput);