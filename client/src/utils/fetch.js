import 'whatwg-fetch';

export const get = query => {
	return fetch(query, { credentials: 'include' }).then(response => {
		if (response.status !== 200)
			throw new Error();
		return response.json();
	});
}

export const httpDelete = query => {
	return fetch(query, { credentials: 'include', method: 'DELETE' }).then(response => {
		if (response.status !== 200)
			throw new Error();
		return response.json();
	})
}

export const post = (query, body) => {
	return fetch(query, {
		credentials: 'include',
		method: 'POST',
		body: JSON.stringify(body),
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		},
	})
	.then(response => {
		if (response.status !== 200)
			throw new Error();
		return response.json()
	});
}

export const postForm = (query, body) => {
	const formData = new FormData();

	for (let key in body) {
		if (body[key].constructor && body[key].constructor === FileList) {
			for (let i = 0; i < body[key].length; i++) {
				formData.append(key, body[key][0]);
			}
		} else {
			formData.append(key, body[key]);
		}
	}

	return fetch(query, {
		credentials: 'include',
		method: 'POST',
		body: formData,
	}).then(response => response.status === 200);
}