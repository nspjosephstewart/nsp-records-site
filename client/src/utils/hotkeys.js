import keyboard from 'keyboardjs';

export const bindKeys = hotKeys => {
	for (let key in hotKeys) {
		keyboard.bind(key, hotKeys[key]);
	}
}

export const unbindKeys = hotKeys => {
	for (let key in hotKeys) {
		keyboard.unbind(key, hotKeys[key]);
	}
}